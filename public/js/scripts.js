function subscribe() {
    if ($('#name').val() != '' && $('#email').val() != '') {
        $.ajax({
            url: '/subscribe',
            method: 'post',
            data: {
                name: $('#name').val(),
                email: $('#email').val()
            },
            headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            success: function (response) {
                $('#success-msg').show(500);
                $('#error-msg').hide();
            }
        });
    } else {
        $('#error-msg').show();
        $('#success-msg').hide();
    }
}

function subscribe2() {
    if ($('#email2').val() != '') {
        $.ajax({
            url: '/subscribe',
            method: 'post',
            data: {
                name: 'visitor',
                email: $('#email2').val()
            },
            headers:
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            success: function (response) {
                $('.response').html('Successfully saved!');
            }
        });
    }
}

function refreshCaptcha() {
    $.ajax({
        url: "/refreshcaptcha",
        type: 'get',
        dataType: 'html',
        success: function (json) {
            $('.captcha span').html(json);
        },
        error: function (data) {
            alert('Try Again.');
        }
    });
}

function addComment(id) {
    if ($('#comment').val() != '') {

        $.ajax({
                type: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/comment",
                data: {// change data to this object
                    _token: $('meta[name="csrf-token"]').attr('content'),
                    comment: $('#comment').val(),
                    post_id: id,
                    rating: $('input:radio[name=rating]:checked').val()
                }
                ,
                dataType: 'html',
                success: function (resultData) {
                    if (resultData == 'false') {
                        window.location.href = "/login";
                    } else {
                        $('#comment-list').append(resultData);
                        $('#comment').val('');
                    }
                }
            }
        );
    }
}

function saveComment(id) {
    if ($('#comment_' + id).val() != '') {

        $.ajax({
                type: "PUT",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/comment/" + id,
                data: {// change data to this object
                    comment: $('#comment_' + id).val(),
                },
                success: function (resultData) {
                    if (resultData == 'false') {
                        window.location.href = "/login";
                    } else {
                        $('#commentp_' + id).text(JSON.parse(resultData));
                        cancelComment(id);
                    }
                }
            }
        );
    }
}

function deleteComment(id) {
    var r = confirm('Are you sure ?');
    if (r) {
        $.ajax({
                type: "delete",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "/comment/" + id,
                success: function (resultData) {
                    if (resultData == 'false') {
                        window.location.href = "/login";
                    } else {
                        $('#li_' + id).remove();
                    }
                }
            }
        );
    }
}

function editComment(id) {
    $('#comment_' + id).focus();
    $('#edit-box_' + id).removeClass('hidden');
}

function cancelComment(id) {
    $('#edit-box_' + id).addClass('hidden');
    $('#comment_' + id).val($('#commentp_' + id).text());
}

$(document).on('keydown', '.comment_box', function (e) {
    if (e.which === 13) {
        $('#' + $(this).attr('id') + '_save').click();
    }
});