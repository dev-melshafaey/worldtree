<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'localization'
], function () {
   
    Route::group([
        'prefix' => 'auth'
    ], function () {
        Route::post('login', 'Api\AuthController@login');
        Route::post('signup', 'Api\AuthController@signup');
        Route::post('password/reset', 'Api\AuthController@forgotPassword');

        Route::group([
            'middleware' => 'auth:api'
        ], function () {
            Route::get('logout', 'Api\AuthController@logout');
            Route::get('profile', 'Api\AuthController@user');
            Route::post('password/update', 'Api\AuthController@changePassword');
        });
    });

    Route::group([
        'prefix' => 'profile'
    ], function () {
        Route::group([
            'middleware' => 'auth:api'
        ], function () {
            Route::post('/', 'Api\ProfileController@updateProfile');
            Route::get('/countries', 'Api\ProfileController@getCountries');
        });
    });

    Route::group([
        
    ], function () {
        Route::group([
            'middleware' => 'auth:api'
        ], function () {
            Route::get('notifications', 'Api\MainController@getNotifications');
            Route::get('blocks', 'Api\MainController@getBlocks');
            Route::get('events', 'Api\MainController@getEvents');
            Route::get('stories', 'Api\MainController@getStories');
            Route::get('articles', 'Api\MainController@getArticles');
            Route::get('categories', 'Api\MainController@getCategories');
            Route::get('contests', 'Api\MainController@getContests');
            Route::get('timetable', 'Api\MainController@getTimetable');
            Route::post('eksab', 'Api\MainController@eksab');
        });
    });
});
