<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::group(
        [
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'GetSetLocale']
        ], function() {
    /** LOCALIZED ROUTES * */
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/about', 'HomeController@about');
    Route::get('/eksab', 'HomeController@eksab');

    Route::get('/contact', 'HomeController@contact');

    Route::get('/blog', 'HomeController@blog');

    Route::get('/gallery', 'HomeController@gallery');

    Route::get('/gallery/{id}', 'HomeController@image');

    Route::get('/blog/{id}', 'HomeController@post');

    Auth::routes();

    Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

    Route::get('account', 'HomeController@account')->name('account');
    Route::get('tutorial', 'HomeController@tutorial')->name('tutorial');
    Route::get('timetable', 'HomeController@timetable')->name('timetable');
    Route::get('faq', 'HomeController@faq')->name('faq');
});
Route::get('/refreshcaptcha', 'Auth\RegisterController@refreshCaptcha');
Route::post('/subscribe', 'HomeController@subscribe');
Route::post('/contact', 'HomeController@message');
Route::post('/comment', 'HomeController@comment');
Route::put('/comment/{id}', 'HomeController@editComment');
Route::delete('/comment/{id}', 'HomeController@deleteComment');
Route::post('/account', 'HomeController@saveAccount')->name('saveAccount');
Route::post('/eksab', 'HomeController@saveEksab')->name('saveEksab');




