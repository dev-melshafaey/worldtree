<?php

namespace App\Observers;

use App\Article;

class ArticleObserver {

    /**
     * Listen to the Article created event.
     *
     * @param  \App\Article  $article
     * @return void
     */
    public function creating(Article $article) {
        $article->user_id = auth()->id();
    }

    public function updating(Article $article) {
        $article->user_id = auth()->id();
    }

    /**
     * Listen to the Article deleting event.
     *
     * @param  \App\Article  $article
     * @return void
     */
    public function deleting(Article $article) {
        //
    }

}
