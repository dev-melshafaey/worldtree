<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Story extends Model
{
    use Translatable;

    protected $translatable = ['title','description'];

    public function getImagesAttribute($values)
    {
        $values = json_decode($values);
        foreach($values as $i => $value){
            $values[$i] = \Storage::url($value);
        }
        return json_encode($values);
    }
}
