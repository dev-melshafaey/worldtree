<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class GalleryImage extends Model
{
    use Translatable;

    protected $translatable = ['title','text'];
    
    public function category() {
        return $this->belongsTo('App\GalleryCategory','category_id');
    }

    public function getPictureAttribute($value)
    {
        return \Storage::url($value);
    }
}
