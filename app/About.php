<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class About extends Model
{
    protected $table = 'about';
    public $timestamps = false;
    use Translatable;

    protected $translatable = ['title','text'];
}
