<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Article extends Model
{

    use Translatable;

    protected $translatable = ['title', 'body', 'author_name', 'author_description'];

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function views()
    {
        return $this->hasMany(ArticleView::class);
    }

    public function getAvgRatingsAttribute()
    {
        return $this->ratings->avg('rating');
    }

    public function getPictureAttribute($value)
    {
        return \Storage::url($value);
    }
}
