<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Package extends Model {

    use Translatable;

    protected $translatable = ['title', 'name', 'button_text','text1','text2','text3','text4','text5'];

}
