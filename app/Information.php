<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use TCG\Voyager\Traits\Spatial;

class Information extends Model
{
    use Spatial;
    use Translatable;

    protected $translatable = ['address'];
    protected $spatial = ['map'];
    public $timestamps = false;

}
