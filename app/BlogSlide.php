<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class BlogSlide extends Model {

    use Translatable;

    protected $translatable = ['title1', 'title2'];

}
