<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class MainBlock extends Model {

    use Translatable;

    protected $translatable = ['title', 'body', 'button_text'];

    public function getPictureAttribute($value)
    {
        return \Storage::url($value);
    }

}
