<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EksabBill extends Model
{
    protected $fillable = ['first_name', 'last_name','national_id','bill','id_pic'];
}
