<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\EksabBill;
use App\Faq;
use App\Http\Helpers\LangHelper;
use App\PagesBackground;
use App\Subscriber;
use App\Timetable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function get_data()
    {
        $info = \App\Information::with('translations')->first()->toArray();
        $info_data = LangHelper::translate($info, \App::getLocale());

        $top_posts = \App\Article::with('translations')->limit(3)
            ->orderBy('post_date', 'desc')->get()->toArray();
        $top_posts_data = LangHelper::translate($top_posts, \App::getlocale());

        $images = \App\GalleryImage::with('translations')->with('category')
            ->get()->toArray();
        $images_data = LangHelper::translate($images, \App::getlocale());

        return [$info_data, $top_posts_data, $images_data];
    }

    public function get_events()
    {
        $events = [];
        $data = \App\Event::all();
        if ($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = \Calendar::event(
                    $value->title, true, new \DateTime($value->start_date),
                    new \DateTime($value->end_date . ' +1 day'),
                    null,
                    // Add color and link on event
                    [
                        'color' => '#46a5e5',
                        'url' => '#',
                    ]
                );
            }
        }
        $calendar = \Calendar::addEvents($events);

        return $calendar;
    }

    public function index()
    {
        $data = $this->get_data();

        $slides = \App\Slide::with('translations')->get()->toArray();
        $slides_data = LangHelper::translate($slides, \App::getLocale());

        $blocks = \App\MainBlock::with('translations')->limit(3)->get()
            ->toArray();
        $blocks_data = LangHelper::translate($blocks, \App::getLocale());

        $cats = \App\GalleryCategory::with('translations')->get()->toArray();
        $cats_data = LangHelper::translate($cats, \App::getlocale());

        $about = \App\About::with('translations')->first()->toArray();
        $about_data = LangHelper::translate($about, \App::getLocale());

        $images = \App\GalleryImage::with('translations')->with('category')
            ->get()->toArray();
        $images_data = LangHelper::translate($images, \App::getlocale());

        $points = \App\AboutPoint::with('translations')->get()->toArray();
        $points_data = LangHelper::translate($points, \App::getlocale());

        $quotes = \App\Quote::with('translations')->get()->toArray();
        $quotes_data = LangHelper::translate($quotes, \App::getlocale());

        $courses = \App\Course::with('translations')->get()->toArray();
        $courses_data = LangHelper::translate($courses, \App::getlocale());

        $events = $this->get_events();

        $stories = \App\Story::with('translations')->first()->toArray();
        $stories_data = LangHelper::translate($stories, \App::getlocale());

        $packages = \App\Package::with('translations')->limit(3)->get()
            ->toArray();
        $packages_data = LangHelper::translate($packages, \App::getlocale());

        $top_posts = Article::with('translations')->withCount('comments')
            ->orderBy('comments_count', 'desc')
            ->limit(3)->get()->toArray();
        $top_posts_data = LangHelper::translate($top_posts, \App::getlocale());

        $bg = PagesBackground::where('slug', 'home')->first();

        return view('layout.home', [
            'data' => $data,
            'header' => true,
            'active' => 'home',
            'slides' => $slides_data,
            'about' => $about_data,
            'blocks' => $blocks_data,
            'cats' => $cats_data,
            'images' => $images_data,
            'points' => $points_data,
            'quotes' => $quotes_data,
            'courses' => $courses_data,
            'calendar' => $events,
            'stories' => $stories_data,
            'packages' => $packages_data,
            'top_posts_data' => $top_posts_data,
            'bg' => $bg
        ]);
    }

    public function about()
    {
        $data = $this->get_data();

        $about = \App\About::with('translations')->first()->toArray();
        $about_data = LangHelper::translate($about, \App::getLocale());

        $quotes = \App\Quote::with('translations')->get()->toArray();
        $quotes_data = LangHelper::translate($quotes, \App::getlocale());

        $points = \App\AboutPoint::with('translations')->get()->toArray();
        $points_data = LangHelper::translate($points, \App::getlocale());

        $bg = PagesBackground::where('slug', 'about')->first();

        return view('layout.about', [
            'data' => $data,
            'about' => $about_data,
            'quotes' => $quotes_data,
            'points' => $points_data,
            'active' => 'about',
            'bg' => $bg,
        ]);
    }

    public function post($id)
    {
        $data = $this->get_data();

        $post = \App\Article::with('translations', 'views')->with([
            'comments' => function ($q) {
                $q->take(10)->with('user');
            },
        ])->find($id);
        //views
        $post->views()->create(['user_id' => \auth()->id()]);

        $posts = $post->toArray();

        $posts_data = LangHelper::translate($posts, \App::getlocale());

        $next = \App\Article::with('translations')
            ->where('id', '>', $posts['id'])->orderBy('id', 'asc')->first();
        if ($next) {
            $next = $next->toArray();
        }
        $next_data = LangHelper::translate($next, \App::getlocale());

        $prev = \App\Article::with('translations')
            ->where('id', '<', $posts['id'])->orderBy('id', 'desc')->first();
        if ($prev) {
            $prev = $prev->toArray();
        }
        $prev_data = LangHelper::translate($prev, \App::getlocale());

        $bg = PagesBackground::where('slug', 'blog')->first();

        return view('layout.blog-single', [
            'data' => $data,
            'posts' => $posts_data,
            'next' => $next_data,
            'prev' => $prev_data,
            'active' => 'blog',
            'bg' => $bg,
        ]);
    }

    public function blog()
    {
        $data = $this->get_data();

        $view = 'blog';

        $blog_slides = \App\BlogSlide::with('translations')->get()->toArray();
        $slides = LangHelper::translate($blog_slides, \App::getlocale());

        $top_posts = \App\Article::with('translations')->limit(3)
            ->orderBy('post_date', 'desc')->get()->toArray();
        $top_posts_data = LangHelper::translate($top_posts, \App::getlocale());

        $arr = \App\Article::with('translations')->paginate(10);
        $posts = $arr->toArray();
        $links = $arr->links();
        $posts_data = LangHelper::translate($posts, \App::getlocale());

        $bg = PagesBackground::where('slug', 'blog')->first();

        return view('layout.' . $view, [
            'data' => $data,
            'slides' => $slides,
            'posts' => $posts_data,
            'links' => $links,
            'top_posts_data' => $top_posts_data,
            'active' => 'blog',
            'bg' => $bg,
        ]);
    }

    public function contact()
    {
        $data = $this->get_data();
        $coordinates = \App\Information::first()->getCoordinates();
        $bg = PagesBackground::where('slug', 'contact')->first();

        return view('layout.contact', [
            'data' => $data,
            'point' => $coordinates,
            'active' => 'contact',
            'bg' => $bg,
        ]);
    }

    public function message(\Illuminate\Http\Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
        ]);

        $message = new \App\Message();
        $message->name = $request->name;
        $message->email = $request->email;
        $message->subject = $request->subject;
        $message->message = $request->message;

        $message->save();
        $request->session()->flash('sent', true);

        return redirect('contact');
    }

    public function gallery()
    {
        $data = $this->get_data();

        $cats = \App\GalleryCategory::with('translations')->get()->toArray();
        $cats_data = LangHelper::translate($cats, \App::getlocale());

        $images = \App\GalleryImage::with('translations')->with('category')
            ->get()->toArray();
        $images_data = LangHelper::translate($images, \App::getlocale());
        $bg = PagesBackground::where('slug', 'gallery')->first();

        return view('layout.gallery', [
            'data' => $data,
            'cats' => $cats_data,
            'images' => $images_data,
            'active' => 'gallery',
            'bg' => $bg,
        ]);
    }

    public function image($id)
    {
        $data = $this->get_data();

        $images = \App\GalleryImage::with('translations')->with('category')
            ->get()->toArray();
        $images_data = LangHelper::translate($images, \App::getlocale());

        $img = [];
        foreach ($images_data as $row) {
            if ($row['id'] == $id) {
                $img = $row;
                break;
            }
        }
        $bg = PagesBackground::where('slug', 'gallery')->first();

        return view('layout.gallery_single', [
            'data' => $data,
            'img' => $img,
            'images' => $images_data,
            'active' => 'gallery',
            'bg' => $bg,
        ]);
    }

    public function comment(\Illuminate\Http\Request $request)
    {
        if (Auth::check()) {
            $userId = Auth::id();
            $post = \App\Article::find($request->post_id);

            if ($request->rating) {
                $post->ratings()->create([
                    'user_id' => $userId,
                    'rating' => $request->rating,
                ]);
            }

            $comment = $post->comments()->create([
                'user_id' => $userId,
                'comment' => $request->comment,
            ]);

            return view('layout.comment', [
                'comment' => $comment,
                'name' => $comment->user->name,
                'date' => $comment->created_at,
            ]);
        } else {
            echo json_encode(false);
        }
    }

    public function editComment(Request $request, $id)
    {
        if (Auth::check()) {
            $comment = Comment::find($id);
            $comment->comment = $request->comment;
            $comment->save();

            return json_encode($comment->comment);
        } else {
            echo json_encode(false);
        }
    }

    public function deleteComment($id)
    {
        if (Auth::check()
            && (\auth()->user()->role->name == 'admin'
                || \auth()->user()->role->name == 'Moderator')
        ) {
            Comment::find($id)->delete();
            echo json_encode(true);
        } else {
            echo json_encode(false);
        }
    }

    public function subscribe(Request $request)
    {
        $subscriber = new Subscriber($request->all());
        $subscriber->save();
    }

    public function account()
    {
        $user = Auth::user();

        return view('layout.account', compact('user'));
    }

    public function saveAccount(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,'
                . $user->id,
            'address' => 'required|string',
            'city' => 'required|string',
            'state' => 'required|string',
            'country' => 'required|string',
            'phone' => 'required',
            'avatar' => 'mimes:jpeg,jpg,png | max:1000',
        ]);

        $password = false;
        if ($request->password) {
            if ($request->password != $request->password_confirmation) {
                return back()->withErrors(["Password didn't match"]);
            } else {
                $password = true;
            }
        }

        $user->name = $request->first_name . " " . $request->last_name;
        $user->email = $request->email;
        if ($password) {
            $user->password = Hash::make($request->password);
        }
        $user->address = $request->address;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->country = $request->country;
        $user->mobile = $request->phone;

        if ($request->hasFile('avatar')) {
            $image = $request->file('avatar');
            $name = str_slug($user->id) . '.'
                . $image->getClientOriginalExtension();
            $destinationPath = public_path('storage/users');
            $image->move($destinationPath, $name);
            $user->avatar = "users/$name";
        }
        $user->save();

        return back()->with('status', 'Saved Successfully!');
    }

    public function eksab()
    {
        return view('layout.eksab', ['active' => 'eksab']);
    }

    public function tutorial()
    {
        $pages = \TCG\Voyager\Models\Page::where('slug', 'tutorial')->with('translations')->first()->toArray();
        $pages_data = LangHelper::translate($pages, \App::getLocale());

        return view('layout.tutorial', ['active' => 'gallery', 'page' => $pages_data]);
    }

    public function timetable()
    {
        $bg = PagesBackground::where('slug', 'timetable')->first();
        $times = Timetable::with('translations')->get()->toArray();
        $times_data = LangHelper::translate($times, \App::getLocale());
        $data = [];
        $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday' , 'sunday'];
        $times = ['8-9', '9-10', '10-11', '11-12', '12-13', '13-14', '14-15', '15-16', '16-17', '17-18', '18-19', '19-20', '20-21'];
        foreach ($times_data as $time) {
            foreach ($days as $day) {
                foreach ($times as $tim) {
                    if ($time['time'] == $tim & $time['day'] == $day) {
                        $data[$tim][$day] = $time['topic'];
                    }
                }
            }
        }

        return view('layout.timetable', ['active' => 'gallery', 'bg' => $bg, 'time' => $data]);
    }

    public function faq()
    {
        $faqs = Faq::with('translations')->get()->toArray();
        $faqs_data = LangHelper::translate($faqs, \App::getLocale());
        $bg = PagesBackground::where('slug', 'faq')->first();
        return view('layout.faq', ['active' => 'faq', 'bg' => $bg, 'faqs' => $faqs_data]);
    }

    public function saveEksab(Request $request)
    {
        $bill = new EksabBill($request->all());
        $bill->user_id = Auth::id();
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'national_id' => 'required|string|max:14|min:14',
            'bill' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id_pic' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('bill')) {
            $image = $request->file('bill');
            $name = str_slug($bill->user_id) . '.'
                . $image->getClientOriginalExtension();
            $destinationPath = public_path('/storage/users/bills');
            $image->move($destinationPath, $name);
            $bill->bill = "users/bills/$name";
        }

        if ($request->hasFile('id_pic')) {
            $image = $request->file('id_pic');
            $name = str_slug($bill->user_id) . '.'
                . $image->getClientOriginalExtension();
            $destinationPath = public_path('/storage/users/id_pic');
            $image->move($destinationPath, $name);
            $bill->id_pic = "users/id_pic/$name";
        }
        $bill->save();

        return back()->with('status', 'Saved Successfully!');
    }
}
