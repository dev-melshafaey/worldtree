<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Monarobase\CountryList\CountryListFacade as Countries;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    
    public function updateProfile(Request $request)
    {
        $request->validate([
            'email' => 'string|email|unique:users',
            'avatar' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $avatar = null;
        
        if ($request->hasFile('avatar')) {
        
            $avatar = $request->file('avatar');
            $name = rand(11111, 99999).'.'.$avatar->getClientOriginalExtension();
            $destinationPath = public_path('/storage/users');
            $avatar->move($destinationPath, $name);
            $avatar = 'users/'.$name;
          }
          
          $data = $request->all();
          $data['avatar'] = $avatar;
          unset($data['password']);

          $user = $request->user();
          $user->update($data);
        
        $user->save();
        return response()->json([
            'message' => __('Successfully updated')
        ], 201);
    }

    public function getCountries(){
        return Countries::getList(app()->getLocale(), 'php');
    }
  
   
}