<?php

namespace App\Http\Controllers\Api;

use App\Article;
use App\EksabBill;
use App\Event;
use App\GalleryCategory;
use App\GalleryImage;
use App\Http\Controllers\Controller;
use App\Notification;
use App\Story;
use App\Timetable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Helpers\LangHelper;

class MainController extends Controller
{
    public function getNotifications(Request $request)
    {
        //$userId = $request->user()->id;
        $notifications = Notification::with('translations')->paginate($request->per_page, ['*'], 'page', $request->current_page);
        $notifications = LangHelper::translate($notifications->toArray(), \App::getlocale());
        return response()->json($notifications);
    }

    public function getBlocks(Request $request)
    {
        $blocks_data = \App\MainBlock::with('translations')->limit(3)->get()->toArray();
        $blocks = LangHelper::translate($blocks_data, \App::getlocale());
        foreach ($blocks as &$block) {
            if (preg_match_all('/\d+/', $block['button_link'], $numbers)) {
                $lastnum = end($numbers[0]);
                $article = Article::with('translations')->withCount('views')->with('ratings')->withCount('ratings')->findOrFail($lastnum);
                $post_data = LangHelper::translate($article->toArray(), \App::getlocale());
                //dd($post_data);
                $block['article'] = $post_data;
            }
        }
        return response()->json($blocks);
    }

    public function getEvents(Request $request)
    {
        $now = Carbon::now();
        $month = $request->month ?? $now->month;
        $year = $request->year ?? $now->year;

        $events = Event::with('translations')->whereYear('start_date', $year)->whereMonth('start_date', $month)->paginate($request->per_page, ['*'], 'page', $request->current_page);
        $events = LangHelper::translate($events->toArray(), \App::getlocale());

        return response()->json($events);
    }

    public function getStories(Request $request)
    {
        $stories = Story::with('translations')->paginate($request->per_page, ['*'], 'page', $request->current_page);
        $stories = LangHelper::translate($stories->toArray(), \App::getlocale());
        return response()->json($stories);
    }

    public function getArticles(Request $request)
    {
        $articles = Article::with('translations')->withCount('views')->with('ratings')->withCount('ratings')->paginate($request->per_page, ['*'], 'page', $request->current_page);
        $articles = LangHelper::translate($articles->toArray(), \App::getlocale());
        return response()->json($articles);
    }

    public function getCategories(Request $request)
    {
        $events = GalleryCategory::with('translations')->paginate($request->per_page, ['*'], 'page', $request->current_page);
        $events = LangHelper::translate($events->toArray(), \App::getlocale());
        return response()->json($events);
    }

    public function getContests(Request $request)
    {
        if ($request->category_id) {
            $contests = GalleryImage::with('translations')->where('category_id', $request->category_id)->paginate($request->per_page, ['*'], 'page', $request->current_page);
        } else {
            $contests = GalleryImage::with('translations')->paginate($request->per_page, ['*'], 'page', $request->current_page);
        }
        $contests = LangHelper::translate($contests->toArray(), \App::getlocale());
        return response()->json($contests);
    }

    public function getTimeTable(Request $request)
    {
        $times = Timetable::with('translations')->paginate($request->per_page, ['*'], 'page', $request->current_page);
        $times = LangHelper::translate($times->toArray(), \App::getlocale());
        return response()->json($times);
    }

    public function eksab(Request $request)
    {
        $bill = new EksabBill($request->all());
        $bill->user_id = $request->user()->id;
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'national_id' => 'required|string|max:14|min:14',
            'bill' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'id_pic' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('bill')) {
            $image = $request->file('bill');
            $name = str_slug($bill->user_id) . '.'
                . $image->getClientOriginalExtension();
            $destinationPath = public_path('/storage/users/bills');
            $image->move($destinationPath, $name);
            $bill->bill = "users/bills/$name";
        }

        if ($request->hasFile('id_pic')) {
            $image = $request->file('id_pic');
            $name = str_slug($bill->user_id) . '.'
                . $image->getClientOriginalExtension();
            $destinationPath = public_path('/storage/users/id_pic');
            $image->move($destinationPath, $name);
            $bill->id_pic = "users/id_pic/$name";
        }
        $bill->save();

        return response()->json([
            'message' => __('Successfully saved')
        ]);
    }
}
