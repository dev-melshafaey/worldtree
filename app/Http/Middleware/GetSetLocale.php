<?php

namespace App\Http\Middleware;

use Closure;

class GetSetLocale {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        if (\App::getLocale() == 'ar') {
            config(['config.ST_LNG' => 'rtl']);
        } else {
            config(['config.ST_LNG' => 'ltr']);
        }
        return $next($request);
    }

}
