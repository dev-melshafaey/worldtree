<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;

class LangHelper 
{
    public static function translate($data,$locale = ''){
        
        if(!$data){
            return false ;
        }
        if(isset($data[0])){
            foreach($data as $key => $single){
                
                foreach($single as $pointer => $value){
                    if(is_array($value) AND $pointer != "translations"){
                        $data[$key][$pointer] = self::translate($data[$key][$pointer],$locale);
                    }
                }

                if(isset($single['translations']) AND !empty($single['translations'])){
                  $single_keys = array_keys($single);
                  foreach($single['translations'] as $translations){
                      if(in_array($translations['column_name'],$single_keys) AND $translations['locale'] == $locale){
                        if(!empty($translations['value'])){
                            $data[$key][$translations['column_name']] = $translations['value'];
                        }
                      }
                  }
                }
            }
        }else{
            
            foreach($data as $pointer => $value){
                if(is_array($value) AND $pointer != "translations"){
                    $data[$pointer] = self::translate($data[$pointer],$locale);
                }
            }
            if(isset($data['translations']) AND !empty($data['translations'])){
                $single_keys = array_keys($data);
                foreach($data['translations'] as $translations){
                    if(in_array($translations['column_name'],$single_keys) AND $translations['locale'] == $locale){
                        if(!empty($translations['value'])){
                            $data[$translations['column_name']] = $translations['value'];
                        }
                    }
                }
              }
        }
        

        return $data ;
    }
}
