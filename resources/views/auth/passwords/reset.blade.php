@extends('index')

@section('content')

<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">

        <div class="row">
            <form method="POST" action="{{ route('password.request') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">
                <div class="col-sm-12">


                    <div class="form-group validate-required validate-email" id="billing_email_field">
                        <label for="billing_email" class="control-label">
                            <span class="grey">{{__('custom.form.email')}} </span>
                            <span class="required">*</span>
                        </label>

                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif

                    </div>

                </div>

                <div class="col-sm-6">


                    <div class="form-group validate-required validate-email" id="billing_email_field">
                        <label for="billing_email" class="control-label">
                            <span class="grey">{{__('custom.form.pass')}} </span>
                            <span class="required">*</span>
                        </label>

                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif

                    </div>

                </div>

                <div class="col-sm-6">


                    <div class="form-group validate-required validate-email" id="billing_email_field">
                        <label for="billing_email" class="control-label">
                            <span class="grey">{{__('custom.form.pass')}} </span>
                            <span class="required">*</span>
                        </label>

                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                    </div>

                </div>

                <div class="col-sm-12">

                    <button type="submit" class="btn btn-primary">
                        {{ __('Reset Password') }}
                    </button>


                </div>

            </form>


        </div>
    </div>
</section>
@endsection
