@extends('index')

@section('content')

<section class="ls section_padding_top_100 section_padding_bottom_100">
    <div class="container">

        <div class="row">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <form method="POST" action="{{ route('password.email') }}">
                @csrf


                <div class="col-sm-12">


                    <div class="form-group validate-required validate-email" id="billing_email_field">
                        <label for="billing_email" class="control-label">
                            <span class="grey">{{__('custom.form.email')}} </span>
                            <span class="required">*</span>
                        </label>

                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif

                    </div>

                </div>

                <div class="col-sm-12">

                    <button type="submit" class="btn btn-primary">
                        {{ __('Send Password Reset Link') }}
                    </button>


                </div>

            </form>


        </div>
    </div>
</section>
@endsection
