@extends('index')

@section('content')
    <section class="ls section_padding_top_100 section_padding_bottom_100">
        <div class="container">

            <div class="row">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('warning'))
                    <div class="alert alert-warning">
                        {{ session('warning') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="col-sm-12">

                        <div class="form-group validate-required validate-email" id="billing_email_field">
                            <label for="billing_email" class="control-label">
                                <span class="grey">{{__('custom.form.email')}} </span>
                                <span class="required">*</span>
                            </label>

                            <input id="email" type="email"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"
                                   value="{{ old('email') }}" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                            @endif

                        </div>

                    </div>


                    <div class="col-sm-12">


                        <div class="form-group" id="billing_password_field">
                            <label for="billing_password" class="control-label">
                                <span class="grey">{{__('custom.form.pass')}}</span>
                                <span class="required">*</span>
                            </label>

                            <input id="password" type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                            @endif

                        </div>
                        <div class="form-group">
                            <label>
                                <input type="checkbox"
                                       name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('custom.form.remember_me') }}
                            </label>
                        </div>

                    </div>


                    <div class="col-sm-12">

                        <button type="submit" class="btn btn-primary">
                            {{ __('custom.menu.login') }}
                        </button>

                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            {{ __('custom.forgot_password?') }}
                        </a>


                    </div>

                </form>


            </div>
        </div>
    </section>
@endsection
