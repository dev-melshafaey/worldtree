@extends('index')

@section('content')
    <section class="page_breadcrumbs ds parallax section_padding_75">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="weight-black">{{__('custom.menu.myAccount')}}</h1>
                    <ol class="breadcrumb darklinks grey">
                        <li>
                            <a href="{{url('/')}}">
                                {{__('custom.menu.home')}}
                            </a>
                        </li>

                        <li class="active"> {{__('custom.menu.myAccount')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>


    <section class="ls section_padding_top_100 section_padding_bottom_100">
        <div class="container">

            <div class="row">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form class="shop-register" enctype="multipart/form-data" role="form" method="post"
                      action="{{route('saveAccount')}}">
                    @csrf
                    <div class="col-sm-6">
                        <div class="form-group validate-required" id="billing_first_name_field">
                            <label for="billing_first_name" class="control-label">
                                <span class="grey">{{__('custom.form.fname')}} </span>
                                <span class="required">*</span>
                            </label>

                            <input type="text"
                                   class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                   required="" name="first_name" value="{{ split_name($user->name)[0] }}">

                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>

                        {{--<div class="form-group" id="billing_company_field">--}}
                        {{--<label for="billing_company" class="control-label">--}}
                        {{--<span class="grey">{{__('custom.form.company')}}</span>--}}
                        {{--</label>--}}

                        {{--<input type="text" class="form-control " name="company" id="company" value="">--}}

                        {{--</div>--}}


                    </div>

                    <div class="col-sm-6">
                        <div class="form-group validate-required" id="billing_last_name_field">
                            <label for="billing_last_name" class="control-label">
                                <span class="grey">{{__('custom.form.lname')}} </span>
                                <span class="required">*</span>
                            </label>

                            <input type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                   name="last_name" required="" value="{{ split_name($user->name)[1]  }}">

                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>

                    <div class="col-sm-12">
                        <div class="form-group validate-required validate-email" id="billing_email_field">
                            <label for="billing_email" class="control-label">
                                <span class="grey">{{__('custom.form.email')}} </span>
                                <span class="required">*</span>
                            </label>

                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   name="email" value="{{$user->email }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group address-field validate-required" id="billing_address_fields">
                            <label for="billing_address_1" class="control-label">
                                <span class="grey">{{__('custom.form.address')}} </span>
                                <span class="required">*</span>
                            </label>


                            <input type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                   name="address" value="{{ $user->address }}" required>

                            @if ($errors->has('address'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="form-group address-field validate-required" id="billing_city_field">
                            <label for="billing_city" class="control-label">
                                <span class="grey">{{__('custom.form.city')}} </span>
                                <span class="required">*</span>
                            </label>

                            <input type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}"
                                   name="city" value="{{ $user->city }}" required>

                            @if ($errors->has('city'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('city') }}</strong>
                                </span>
                            @endif

                        </div>

                        {{--<div class="form-group address-field validate-required validate-postcode" id="billing_postcode_field">--}}
                        {{--<label for="billing_postcode" class="control-label">--}}
                        {{--<span class="grey">{{__('custom.form.postal')}} </span>--}}
                        {{--<span class="required">*</span>--}}
                        {{--</label>--}}

                        {{--<input type="text" class="form-control{{ $errors->has('postcode') ? ' is-invalid' : '' }}" name="postcode" value="{{ old('postcode') }}" required>--}}

                        {{--@if ($errors->has('postcode'))--}}
                        {{--<span class="invalid-feedback">--}}
                        {{--<strong>{{ $errors->first('postcode') }}</strong>--}}
                        {{--</span>--}}
                        {{--@endif--}}

                        {{--</div>--}}

                        <div class="form-group validate-required validate-phone" id="billing_phone_field">
                            <label for="billing_phone" class="control-label">
                                <span class="grey">{{__('custom.form.tel')}} </span>
                                <span class="required">*</span>
                            </label>

                            <input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                   name="phone" value="{{ $user->mobile  }}" required>

                            @if ($errors->has('phone'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif

                        </div>


                        <div class="form-group" id="billing_password_field">
                            <label for="billing_password" class="control-label">
                                <span class="grey">{{__('custom.form.pass')}}</span>
                                <span class="required">Leave empty if you don't want to change</span>
                            </label>

                            <input id="password" type="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   name="password">

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>

                    </div>

                    <div class="col-sm-6">

                        <div class="form-group">
                            <label for="billing_state" class="control-label">
                                <span class="grey">{{__('custom.form.state')}} </span>
                                <span class="required">*</span>
                            </label>
                            <input type="text" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}"
                                   name="state" value="{{ $user->state }}">

                            @if ($errors->has('state'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('state') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="billing_country" class="control-label">
                                <span class="grey">{{__('custom.form.country')}} </span>
                                <span class="required">*</span>
                            </label>

                            <select id="country" name="country" class="form-control">
                                @foreach(getCountries() as $country)
                                    <option {{$country==$user->country ? 'selected': ''}} value="{{$country}}">{{$country}}</option>
                                @endforeach
                            </select>
                        </div>

                        {{--<div class="form-group validate-required validate-fax" id="billing_fax_field">--}}
                        {{--<label for="billing_fax" class="control-label">--}}
                        {{--<span class="grey">{{__('custom.form.fax')}}</span>--}}

                        {{--</label>--}}

                        {{--<input type="text" class="form-control " name="fax" id="billing_fax" value="">--}}

                        {{--</div>--}}


                        <div class="form-group" id="billing_password2_field">
                            <label for="billing_password2" class="control-label">
                                <span class="grey">{{__('custom.form.con-pass')}}</span>
                                <span class="required">Leave empty if you don't want to change</span>
                            </label>

                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="billing_state" class="control-label">
                                <span class="grey">{{__('custom.form.picture')}} </span>
                            </label>
                            <input type="file" name="avatar" class="form-control">
                            @if($user->avatar)
                                <a href="{{\Illuminate\Support\Facades\Storage::url($user->avatar)}}"><img
                                            src="{{\Illuminate\Support\Facades\Storage::url($user->avatar)}}"
                                            width="50"></a>
                            @endif
                            @if ($errors->has('avatar'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('avatar') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>


                    <div class="col-sm-12">
                        <button type="submit"
                                class="theme_button wide_button color1">{{__('custom.form.save')}}</button>
                        {{--<button type="reset" class="theme_button wide_button">{{__('custom.form.clear')}}</button>--}}

                    </div>

                </form>
            </div>
        </div>
    </section>
@endsection
