@extends('index')
@section('content')

<section class="page_breadcrumbs ds parallax section_padding_75" style="background-image: url({{asset(\Illuminate\Support\Facades\Storage::url($bg->path))}})">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1 class="weight-black">{{__('custom.menu.gallery')}}</h1>
                <ol class="breadcrumb darklinks grey">
                    <li>
                        <a href="{{url('/')}}">
                            {{__('custom.menu.home')}}
                        </a>
                    </li>

                    <li class="active">{{__('custom.menu.gallery')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls page_portfolio section_padding_top_150 columns_margin_bottom_30 section_padding_bottom_120">
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-sm-push-1">

                <div class="filters isotope_filters text-center">
                    <a href="#" data-filter="*" class="selected">All</a>
                    @foreach($cats as $cat)
                    <a href="#" data-filter=".{{$cat['slug']}}">{{$cat['name']}}</a>
                    @endforeach
                </div>

                <div class="isotope_container isotope row masonry-layout columns_margin_top_0 columns_margin_bottom_60" data-filters=".isotope_filters">
                    @foreach($images as $img)
                    <div class="isotope-item col-lg-12 col-md-12 col-sm-12 {{$img['category']['slug']}}">
                        <div class="vertical-item content-padding gallery-extended-item gallery-item with_shadow text-center">
                            <div class="item-media">
                                <a href="{{url('gallery/'.$img['id'])}}"><img src="{{$img['picture']}}" alt="" /></a>
{{--                                <img src="{{$img['picture']}}" alt="" />--}}
{{--                                <div class="media-links">--}}
{{--                                    <a class="abs-link prettyPhoto" title="" data-gal="prettyPhoto[gal]" href="{{$img['picture']}}"></a>--}}
{{--                                </div>--}}
                            </div>
                            <div class="item-content">
<!--                                <span class="categories-links small-text">
                                    <a rel="category" href="gallery-fullwidth-4-cols.html">Books</a>
                                </span>-->
                                <h3 class="item-title">
                                    <a href="{{url('gallery/'.$img['id'])}}">{{$img['title']}}</a>
                                </h3>
                                {!! substr($img['text'],0,100) !!}
                            </div>
                        </div>
                    </div>
                    @endforeach

                        <a href="{{route('tutorial')}}"
                           class="theme_button inverse topmargin_10">{{__('custom.form.tutorial')}}</a>
                        <a href="{{route('timetable')}}"
                           class="theme_button inverse topmargin_10">{{__('custom.form.timetable')}}</a>
                </div>
                <!-- eof .isotope_container.row -->

                <div class="row" style="display: none">
                    <div class="col-sm-12 text-center">
                        <img src="{{asset('img/loading.png')}}" alt="" class="fa-spin">
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

@endsection     