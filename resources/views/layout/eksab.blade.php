@extends('index')

@section('content')
    <section class="page_breadcrumbs ds parallax section_padding_75">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="weight-black">{{__('custom.menu.eksab')}}</h1>
                    <ol class="breadcrumb darklinks grey">
                        <li>
                            <a href="{{url('/')}}">
                                {{__('custom.menu.home')}}
                            </a>
                        </li>

                        <li class="active"> {{__('custom.menu.eksab')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>


    <section class="ls section_padding_top_100 section_padding_bottom_100">
        <div class="container">

            <div class="row">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br/>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form class="shop-register" enctype="multipart/form-data" role="form" method="post"
                      action="{{route('saveEksab')}}">
                    @csrf
                    <div class="col-sm-6">
                        <div class="form-group validate-required" id="billing_first_name_field">
                            <label for="billing_first_name" class="control-label">
                                <span class="grey">{{__('custom.form.fname')}} </span>
                                <span class="required">*</span>
                            </label>

                            <input type="text"
                                   class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                   required="" name="first_name" value="{{old('first_name')}}">

                            @if ($errors->has('first_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>

                        {{--<div class="form-group" id="billing_company_field">--}}
                        {{--<label for="billing_company" class="control-label">--}}
                        {{--<span class="grey">{{__('custom.form.company')}}</span>--}}
                        {{--</label>--}}

                        {{--<input type="text" class="form-control " name="company" id="company" value="">--}}

                        {{--</div>--}}


                    </div>

                    <div class="col-sm-6">
                        <div class="form-group validate-required" id="billing_last_name_field">
                            <label for="billing_last_name" class="control-label">
                                <span class="grey">{{__('custom.form.lname')}} </span>
                                <span class="required">*</span>
                            </label>

                            <input type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                   name="last_name" required="" value="{{old('last_name')}}">

                            @if ($errors->has('last_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif

                        </div>

                    </div>

                    <div class="col-sm-12">
                        <div class="form-group validate-required validate-email" id="billing_email_field">
                            <label for="billing_email" class="control-label">
                                <span class="grey">{{__('custom.form.id')}} </span>
                                <span class="required">*</span>
                            </label>

                            <input type="text" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}"
                                   name="national_id" value="{{old('national_id')}}" required>

                            @if ($errors->has('id'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="billing_state" class="control-label">
                                <span class="grey">{{__('custom.form.bill')}} </span>
                            </label>
                            <input type="file" name="bill" class="form-control">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="billing_state" class="control-label">
                                <span class="grey">{{__('custom.form.id_pic')}} </span>
                            </label>
                            <input type="file" name="id_pic" class="form-control">

                        </div>
                    </div>


                    <div class="col-sm-12">
                        <button type="submit"
                                class="theme_button wide_button color1 float-right">{{__('custom.form.save')}}</button>
                        {{--<button type="reset" class="theme_button wide_button">{{__('custom.form.clear')}}</button>--}}

                    </div>

                </form>
            </div>
        </div>
    </section>
@endsection
