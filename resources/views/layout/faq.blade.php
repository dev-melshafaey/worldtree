@extends('index')
@section('content')
    <section class="page_breadcrumbs ds parallax section_padding_75" style="background-image: url({{asset(\Illuminate\Support\Facades\Storage::url($bg->path))}})">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="weight-black">{{__('custom.menu.faq')}}</h1>
                    <ol class="breadcrumb darklinks grey">
                        <li>
                            <a href="{{url('/')}}">
                                {{__('custom.menu.home')}}
                            </a>
                        </li>

                        <li class="active">{{__('custom.menu.faq')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section class="ls section_padding_120">
        <div class="container">

            <div class="row">

                @foreach($faqs as $i => $faq)
                <div class="col-sm-6">
                    <div class="panel-group" id="accordion{{++$i}}">

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion1" href="#collapse{{$i}}">
                                        <i class="fa fa-question-circle-o highlight"></i>
                                        {{$faq['question']}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{$i}}" class="panel-collapse collapse {{$i==1 ? 'in' : ''}}">
                                <div class="panel-body">
                                    {{$faq['answer']}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                @endforeach

            </div>

        </div>
    </section>
@endsection
