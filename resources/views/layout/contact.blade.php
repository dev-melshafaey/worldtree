@extends('index')
@section('content')

<section class="page_breadcrumbs ds parallax section_padding_75" style="background-image: url({{asset(\Illuminate\Support\Facades\Storage::url($bg->path))}})">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1 class="weight-black">{{__('custom.menu.contact')}}</h1>
                <ol class="breadcrumb darklinks grey">
                    <li>
                        <a href="{{url('/')}}">
                            {{__('custom.menu.home')}}
                        </a>
                    </li>

                    <li class="active">{{__('custom.menu.contact')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls section_padding_top_100 section_padding_bottom_75">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div id="map" style="width: 100%;height: 450px;text-align: center">


                </div>


            </div>
        </div>

        <div class="row topmargin_40">
            <div class="col-sm-4 to_animate" data-animation="pullDown">
                <div class="teaser text-center">
                    <div class="teaser_icon highlight size_normal">
                        <i class="rt-icon2-phone5"></i>
                    </div>

                    <p>
                        <span class="grey"></span> {{$data[0]['mobile']}}
                        <br>

                    </p>

                </div>
            </div>
            <div class="col-sm-4 to_animate" data-animation="pullDown">
                <div class="teaser text-center">
                    <div class="teaser_icon highlight size_normal">
                        <i class="rt-icon2-location2"></i>
                    </div>

                    <p>
                        {{$data[0]['address']}}
                    </p>

                </div>
            </div>
            <div class="col-sm-4 to_animate" data-animation="pullDown">
                <div class="teaser text-center">
                    <div class="teaser_icon highlight size_normal">
                        <i class="rt-icon2-mail"></i>
                    </div>

                    <p>{{$data[0]['email']}}</p>

                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-sm-12 to_animate">
                @if(session()->has('sent'))
                <div class="alert alert-success"> 
                     {{__('custom.sent')}}
                </div>
                @endif
                <form class="contact-form" method="post" action="{{url('contact')}}">
                    <div class="row columns_padding_5">
                        @csrf
                        <div class="col-sm-6">
                            <p class="contact-form-name">
                                <label for="name"> {{__('custom.form.name')}}
                                    <span class="required">*</span>
                                </label>
                                <input type="text" required="true" size="30" value="" name="name" id="name" class="form-control" placeholder="{{__('custom.form.name')}}">
                                @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </p>
                            <p class="contact-form-email">
                                <label for="email">{{__('custom.form.email')}}
                                    <span class="required">*</span>
                                </label>
                                <input type="email" required="" size="30" value="" name="email" id="email" class="form-control" placeholder="{{__('custom.form.email')}}">
                                @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </p>
                            <p class="contact-form-subject">
                                <label for="subject">{{__('custom.form.subject')}}
                                    <span class="required">*</span>
                                </label>
                                <input type="text" size="30" value="" name="subject" id="subject" class="form-control" placeholder="{{__('custom.form.subject')}}">
                            </p>
                        </div>
                        <div class="col-sm-6">

                            <p class="contact-form-message">
                                <label for="message">{{__('custom.form.message')}}</label>
                                <textarea required="true" rows="9" cols="45" name="message" id="message" class="form-control" placeholder="{{__('custom.form.message')}}"></textarea>
                                @if ($errors->has('message'))
                                <span class="invalid-feedback">
                                    <strong class="text-danger">{{ $errors->first('message') }}</strong>
                                </span>
                                @endif
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">

                            <p class="contact-form-submit text-center topmargin_30">
                                <button type="submit" name="contact_submit" class="theme_button color1">{{__('custom.form.send')}}</button>
                            </p>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script async defer src="https://maps.googleapis.com/maps/api/js?key={{ config('voyager.googlemaps.key') }}&callback=initMap"></script>
<script type="application/javascript">
    function initMap() {

    var center = {lat: {{ $point[0]['lat'] }}, lng: {{ $point[0]['lng'] }}};


    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: {{ config('voyager.googlemaps.zoom') }},
    center: center
    });
    var markers = [];

    var marker = new google.maps.Marker({
    position: {lat: {{ $point[0]['lat'] }}, lng: {{ $point[0]['lng'] }}},
    map: map,
    draggable: true
    });
    markers.push(marker);


    google.maps.event.addListener(marker,'dragend',function(event) {
    document.getElementById('lat').value = this.position.lat();
    document.getElementById('lng').value = this.position.lng();
    });
    }
</script>


@endsection