@php
    $data  = app(App\Http\Controllers\HomeController::class)->get_data();
@endphp
<header class="page_header header_white affix-header table_section columns_padding_0 toggle_menu_right">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-xs-6">
                <a href="{{url('/')}}" class="logo">
                    <img src="{{asset('images/logo.png')}}" alt="World Tree">
                </a>
                <span class="toggle_menu">
                    <span></span>
                </span>
            </div>
            <div class="col-md-6 text-center">
                <!-- main nav start -->
                <nav class="mainmenu_wrapper">
                    <ul class="mainmenu nav sf-menu">
                        <li class="@if(isset($active) && $active == 'home') active @endif">
                            <a href="{{url('/')}}">{{__('custom.menu.home')}}</a>
                        </li>

                        <!-- blog -->
                        <li class="@if(isset($active) && $active == 'blog') active @endif">
                            <a href="{{url('blog')}}">{{__('custom.menu.blog')}}</a>

                        </li>
                        <!-- eof blog -->

                        <!-- gallery -->
                        <li class="@if(isset($active) && $active == 'gallery') active @endif">
                            <a href="{{url('gallery')}}">{{__('custom.menu.gallery')}}</a>

                        </li>
                        <!-- eof Gallery -->

                        <li class="@if(isset($active) && $active == 'about') active @endif">
                            <a href="{{url('about')}}">{{__('custom.menu.about')}}</a>

                        </li>
                        <!-- eof pages -->

                        <li class="@if(isset($active) && $active == 'eksab') active @endif">
                            <a href="{{url('eksab')}}">{{__('custom.menu.eksab')}}</a>
                        </li>

                        <li class="@if(isset($active) && $active == 'faq') active @endif">
                            <a href="{{url('faq')}}">{{__('custom.menu.faq')}}</a>
                        </li>

                        <!-- contacts -->
                        <li class="@if(isset($active) && $active == 'contact') active @endif">
                            <a href="{{url('contact')}}">{{__('custom.menu.contact')}}</a>

                        </li>
                        <!-- eof contacts -->

                        @auth
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                                    {{__('custom.menu.logout')}}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                            <li><a href="{{ route('account') }}"> {{__('custom.menu.account')}}</a></li>
{{--                            <li><a href="{{ url('http://chaty.local/login?id='.auth()->id()) }}"> {{__('custom.menu.chat')}}</a></li>--}}
                            <li><a href="{{ url('http://chat.world-tree.co/login?id='.auth()->id()) }}"> {{__('custom.menu.chat')}}</a></li>
                        @else
                            <li><a href="{{ route('login') }}"> {{ __('custom.menu.login') }}</a></li>
                            <li><a href="{{ route('register') }}"> {{__('custom.menu.register')}}</a></li>
                        @endauth

                        <li>
                            <a rel="alternate"
                               hreflang="{{(\App::getLocale() == 'ar') ? $localeCode = 'en' : $localeCode = 'ar' }}"
                               href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                <span style="letter-spacing: 0">{{ LaravelLocalization::getSupportedLocales()[$localeCode]['native'] }}</span>
                            </a>
                        </li>


                    </ul>
                </nav>
                <!-- eof main nav -->

            </div>
            <div class="col-md-3 text-right">

                <span class="hidden-xs">
                    @if(isset($active))
                        <a href="{{$data[0]['facebook']}}"
                           class="social-icon border-icon rounded-icon color-icon soc-facebook"></a>
                        <a href="{{$data[0]['instagram']}}"
                           class="social-icon border-icon rounded-icon color-icon soc-instagram"></a>
                        <a href="{{$data[0]['youtube']}}"
                           class="social-icon border-icon rounded-icon color-icon soc-youtube"></a>
                    @endif
                </span>
            </div>
        </div>
    </div>
</header>