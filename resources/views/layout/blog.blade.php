@extends('index')
@section('content')

<section class="page_breadcrumbs ds parallax section_padding_75" style="background-image: url({{asset(\Illuminate\Support\Facades\Storage::url($bg->path))}})">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1 class="weight-black">{{__('custom.menu.blog')}}</h1>
                <ol class="breadcrumb darklinks grey">
                    <li>
                        <a href="{{url('/')}}">
                            {{__('custom.menu.home')}}
                        </a>
                    </li>

                    <li class="active">{{__('custom.menu.blog')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>


<section class="ls section_padding_top_150 section_padding_bottom_120">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <article id="blog-gallery-slider" class="carousel slide ds">

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        @foreach($slides as $i => $slide)
                        <div class="item text-center @if($i == 0) {{'active'}} @endif">
                            <div class="vertical-item content-absolute vertical-center">
                                <div class="item-media">
                                    <img src="{{asset('storage/'.$slide['picture'])}}" alt="" />
                                </div>
                                <div class="item-content">
                                    <div class="display_table">
                                        <div class="display_table_cell text-center grey">
                                            <p class="big">{{$slide['title1']}}</p>
                                            <p>{{$slide['title2']}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#blog-gallery-slider" data-slide-to="0" class="active"></li>
                        <li data-target="#blog-gallery-slider" data-slide-to="1"></li>
                        <li data-target="#blog-gallery-slider" data-slide-to="2"></li>
                    </ol>

                </article>
            </div>
        </div>

        <div class="row columns_margin_bottom_30 topmargin_60 bottommargin_40">
            @foreach($top_posts_data as $i => $post)
           
            <div class="col-sm-6 col-md-4">
                <article class="vertical-item content-padding with_shadow text-center">
                    <div class="item-media">
                        <img src="{{$post['picture']}}" alt="">
                    </div>
                    <div class="item-content">
                        <div class="categories-links small-text">
                            <a href="#"></a>
                        </div>
                        <h4 class="entry-title">
                            <a href="{{url('blog/'.$post['id'])}}">{{$post['title']}}</a>
                        </h4>
                        {!! substr($post['body'],0,100) !!}
                    </div>
                </article>
            </div>
           
            @endforeach
        </div>

        <div class="row columns_padding_30">

            <div class="col-sm-10 col-sm-push-1">
                @foreach($posts['data'] as $i => $post)

                <article class="vertical-item content-padding post format-video with_shadow">

                    @if($post['video'] != '')
                    <div class="entry-thumbnail">
                        <div class="embed-responsive embed-responsive-3by2">
                            <a href="{{$post['video']}}" class="embed-placeholder">
                                <img src="{{$post['picture']}}" alt="{{$post['title']}}">
                            </a>
                        </div>
                    </div>
                    @else
                    <div class="item-media entry-thumbnail">
                        <img src="{{$post['picture']}}" alt="{{$post['title']}}">
                    </div>
                    @endif
                    <div class="item-content entry-content">
                        <header class="entry-header">

                            <div class="item-meta small-text">
                                <span>
                                    <time datetime="2016-08-01T15:05:23+00:00" class="entry-date">
                                        {{$post['post_date']}}
                                    </time>
                                </span>

<!--                                <div class="categories-links">
                                    <a href="#">Video</a>
                                </div>-->
                            </div>

                            <h4 class="entry-title">
                                <a href="{{url('blog/'.$post['id'])}}" rel="bookmark" >{{$post['title']}}</a>
                            </h4>

                        </header>
                        <!-- .entry-header -->

                        {!! substr($post['body'],0,100) !!}

                    </div>
                    <!-- .item-content.entry-content -->

                </article>
                <!-- .post -->



                @endforeach


                <div class="row topmargin_50">
                    <div class="col-sm-12 text-center">
                        {{$links}}
                    </div>
                </div>

            </div>
            <!--eof .col-sm-8 (main content)-->


        </div>
    </div>
</section>

@endsection