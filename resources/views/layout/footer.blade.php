@php
$data  = app(App\Http\Controllers\HomeController::class)->get_data();
@endphp
<footer class="page_footer ds section_padding_110 columns_padding_25">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="widget widget_text">
                    <a href="./" class="logo bottommargin_20">
                        <img src="{{asset('images/logo.png')}}" alt="brand">
                    </a>
                    <div class="media thin-media">
                        <div class="media-left fontsize_16">
                            <i class="fa fa-map-marker"></i>
                        </div>
                        <div style="text-align: left" class="media-body address">
                            {{$data[0]['address']}}
                        </div>
                    </div>
                    <div class="media thin-media">
                        <div class="media-left fontsize_16">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="media-body">
                            {{$data[0]['mobile']}}
                        </div>
                    </div>
                    <div class="media thin-media">
                        <div class="media-left fontsize_16">
                            <i class="fa fa-pencil"></i>
                        </div>
                        <div class="media-body darklinks">
                            <a href="#">  {{$data[0]['email']}}</a>
                        </div>
                    </div>
                    <div class="topmargin_25">
                        <a href="{{$data[0]['facebook']}}" class="social-icon color-bg-icon rounded-icon soc-facebook"></a>
                        <a href="{{$data[0]['instagram']}}" class="social-icon color-bg-icon rounded-icon soc-instagram"></a>
                        <a href="{{$data[0]['youtube']}}" class="social-icon color-bg-icon rounded-icon soc-youtube"></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="widget widget_recent_news">
                    <h3 class="widget-title">{{__('custom.latest_news')}}</h3>
                    <ul class="greylinks">

                        @foreach($data[1] as $i => $post)
                        <li>
                            <a href="{{url('blog/'.$post['id'])}}">{{$post['title']}}</a>
                            <time datetime="2015-11-08T15:05:23+00:00" class="entry-date small-text highlight"> {{$post['post_date']}}</time>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="widget widget_mailchimp">

                    <h3 class="widget-title">{{__('custom.gain')}}</h3>

                    <p>{{__('custom.gain_p')}}</p>

                    <form class="signup form-inline">
                        <div class="form-group">
                            <input name="email" type="email" id="email2" required class="mailchimp_email form-control" placeholder="{{__('custom.form.email')}}">
                        </div>
                        <button onclick="subscribe2()" type="button" class="theme_button">
                            <span class="sr-only">{{__('custom.form.send')}}<</span>
                            <i class="fa fa-pencil"></i>
                        </button>
                        <div class="response"></div>
                    </form>

                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="widget widget_gallery">
                    <h3 class="widget-title">{{__('custom.menu.gallery')}}</h3>
                    <ul id="gallery">
                         @foreach($data[2] as $img)
                        <li class="vertical-item">
                            <div class="item-media">
                                <img title="{{$img['title']}}" src="{{$img['picture']}}" alt="{{$img['title']}}" />
                                <div class="media-links">
                                    <a class="abs-link prettyPhoto" title="{{$img['title']}}" href="{{url('gallery/'.$img['id'])}}"></a>
                                </div>
                            </div>
                        </li>
                         @endforeach

                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<section class="page_copyright ls section_padding_40">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <p class="lato small-text">World Tree by Unknown Studio - All Rights Reserved &copy; 2019</p>
            </div>
        </div>
    </div>
</section>