@extends('index')
@section('content')
    <section class="page_breadcrumbs ds parallax section_padding_75" style="background-image: url({{asset(\Illuminate\Support\Facades\Storage::url($page['image']))}})">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="weight-black">{{__('custom.menu.tutorial')}}</h1>
                    <ol class="breadcrumb darklinks grey">
                        <li>
                            <a href="{{url('/')}}">
                                {{__('custom.menu.home')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{url('gallery')}}">
                                {{__('custom.menu.gallery')}}
                            </a>
                        </li>
                        <li class="active">{{__('custom.menu.tutorial')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="ls section_padding_150">
        <div class="container">
            <div class="row topmargin_5">
                <div class="col-sm-12">
                    {!! $page['body'] !!}

                </div>
            </div>

        </div>
    </section>

@endsection