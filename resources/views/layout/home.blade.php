@extends('index')
@section('content')
    <section class="intro_section page_mainslider">
        <div class="flexslider text-nav">
            <ul class="slides">
                @if($slides)
                    @foreach($slides as $slide)
                        <li class="cs">
                            <img src="{{asset('storage/'.$slide['picture'])}}" alt="">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 text-center">
                                        <div class="slide_description_wrapper">
                                            <div class="slide_description">
                                                <p class="big">{{$slide['title1']}}</p>
                                                <p>{{$slide['title2']}}</p>
                                            </div>
                                            <!-- eof .slide_description -->
                                        </div>
                                        <!-- eof .slide_description_wrapper -->
                                    </div>
                                    <!-- eof .col-* -->
                                </div>
                                <!-- eof .row -->
                            </div>
                            <!-- eof .container -->
                        </li>
                    @endforeach
                @endif
            </ul>
        </div>
        <!-- eof flexslider -->
    </section>

    <section class="ls page_highlights columns_margin_bottom_30 columns_margin_top_0 section_padding_top_30">
        <div class="container">
            <div class="row">

                @foreach($blocks as $i => $block)
                    <div class="col-sm-6 col-md-offset-0 col-md-4">
                        <article
                                class="vertical-item content-padding with_padding with_shadow text-center highlight-item ls @if($i==1) default-hovered hovered @endif">
                            <div class="item-media">
                                <img src="{{$block['picture']}}" alt="">
                            </div>
                            <div class="item-content">
                                <h3 class="entry-title2">
                                    <a href="#">{{explode(' ',$block['title'])[0]}}
                                        <br>
                                        <strong class="weight-black">
                                            @if(count(explode(' ',$block['title'])) > 1)
                                                {{explode(' ',$block['title'])[1]}}
                                            @endif
                                        </strong>
                                    </a>
                                </h3>
                                <p>
                                    {{$block['body']}}
                                </p>
                                <a href="{{$block['button_link']}}"
                                   class="theme_button inverse topmargin_10">{{$block['button_text']}}</a>
                            </div>
                        </article>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

    <section class="ls columns_padding_bottom section_padding_top_50 table_section table_section_md">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h3 class="bottommargin_40">
                        <strong class="highlight weight-black fontsize_76">{{explode(' ',$about['title'])[0]}}</strong>
                        @if(count(explode(' ',$about['title'])) > 1)
                            {{explode(' ',$about['title'])[1]}}
                        @endif
                    </h3>
                    <p>
                        {{$about['text']}}
                    </p>
                    <a href="{{url('about')}}" class="theme_button inverse topmargin_30 para_about">{{__('custom.menu.about')}}</a>
                    <p class="lato grey fontsize_20 text-uppercase topmargin_40">
                        {{__('custom.call')}}
                        <br>
                        <span class="fontsize_38 weight-black">{{$data[0]['mobile']}}</span>
                    </p>
                </div>
                <div class="col-md-6 text-center bottommargin_0">
                    <img src="{{asset('storage/'.$about['picture'])}}" alt="">
                </div>
            </div>
        </div>
    </section>

    <section class="ls ms section_padding_150 section_subscribe parallax">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-center">
                    <h2 class="section_header">
                        {{__('custom.gain')}}
                    </h2>
                    <p>
                        {{__('custom.gain_p')}}
                    </p>
                    <h5 style="color: green;display: none" id="success-msg">
                        Saved Successfully!
                    </h5>
                    <h5 style="color: red;display: none" id="error-msg">
                        Both Name and Email are Required!
                    </h5>
                    <div class="row columns_padding_5 topmargin_40">
                        <div class="col-sm-6">
                            <div class="subscribe-form-name">
                                <label for="name" class="sr-only">{{__('custom.form.name')}}
                                    <span class="required">*</span>
                                </label>
                                <input type="text" required aria-required="true" size="30" value="" name="name"
                                       id="name" class="form-control text-center"
                                       placeholder="{{__('custom.form.name')}}">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="subscribe-form-email">
                                <label for="email" class="sr-only">{{__('custom.form.email')}}
                                    <span class="required">*</span>
                                </label>
                                <input type="email" required aria-required="true" size="30" value="" name="email"
                                       id="email" class="form-control text-center"
                                       placeholder="{{__('custom.form.email')}}">
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="subscribe-form-submit topmargin_30">
                                <button type="button" onclick="subscribe()" id="contact_form_submit"
                                        name="contact_submit"
                                        class="theme_button color1">{{__('custom.form.send')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ls columns_padding_0 section_padding_top_65 columns_margin_0">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="filters carousel_filters text-center">
                        <a href="#" data-filter="*" class="selected">All</a>
                        @foreach($cats as $cat)
                            <a href="#" data-filter=".{{$cat['slug']}}">{{$cat['name']}}</a>
                        @endforeach

                    </div>
                    <div class="owl-carousel filterable-carousel bottommargin_0" data-center="true" data-margin="0"
                         data-loop="true" data-autoplay="true" data-filters=".carousel_filters">

                        @foreach($images as $img)
                            <div class="isotope-item {{$img['category']['slug']}}">
                                <div class="vertical-item gallery-item">
                                    <div class="item-media">
                                        <img src="{{$img['picture']}}" alt=""/>
                                        <div class="media-links">
                                            <a class="abs-link prettyPhoto" title=""
                                               href="{{url('gallery/'.$img['id'])}}"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ls section_padding_top_150 section_padding_bottom_120 columns_margin_bottom_30">
        <div class="container">
            <div class="row">
                @foreach($points as $point)
                    <div class="col-sm-6 col-md-3">
                        <div class="teaser text-center">
                            <a class="teaser_icon size_small big border_icon round" href="#">
                                <i class="fa {{$point['icon']}} highlight"></i>
                            </a>
                            <h4 class="text-uppercase">
                                <a href="#">{{explode(' ',$point['title'])[0]}}
                                    <br>
                                    <strong class="weight-black">
                                        @if(count(explode(' ',$point['title'])) > 1)
                                            @foreach(explode(' ',$point['title']) as $index => $title)
                                                @if($index > 0)
                                                    {{$title.' '}}
                                                @endif
                                            @endforeach
                                        @endif
                                    </strong>
                                </a>
                            </h4>
                            <p>
                                {{$point['description']}}
                            </p>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>

    <section class="ds section_padding_150 coaching_courses image_bg muted_image_bg" style="background-image:url({{\Illuminate\Support\Facades\Storage::url($bg->path)}})">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="section_header">{{__('custom.courses1')}}
                        <br>
                        <strong class="weight-black">{{__('custom.courses2')}}</strong>
                    </h2>
                    <div class="owl-carousel offset-nav text-left topmargin_60" data-nav="true">

                        @foreach($courses as $course)
                            <article class="vertical-item content-padding ls">
                                <div class="item-media">
                                    <img src="{{asset('storage/'.$course['picture'])}}" alt="">
                                </div>
                                <div class="item-content">
                                    <h4 class="entry-title">
                                        <a href="#">{{$course['title']}}</a>
                                    </h4>
                                    <div>
                                        <div class="media-left media-middle">
                                            <div class="fontsize_14 round with_background text-center icon">
                                                <i class="fa fa-user black"></i>
                                            </div>
                                        </div>
                                        <div class="media-body media-middle greylinks">
                                            <a class="small-text" href="#">{{$course['user']}}</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="item-footer">
                                    <!--                            <div class="star-rating2">
                                                                    <fieldset>
                                                                        <input type="radio" id="star5" name="rating" value="5" />
                                                                        <label for="star5" title="Outstanding">5 stars</label>
                                                                        <input type="radio" id="star4" name="rating" value="4" />
                                                                        <label for="star4" title="Very Good">4 stars</label>
                                                                        <input type="radio" id="star3" name="rating" value="3" />
                                                                        <label for="star3" title="Good">3 stars</label>
                                                                        <input type="radio" id="star2" name="rating" value="2" checked="" />
                                                                        <label for="star2" title="Poor">2 stars</label>
                                                                        <input type="radio" id="star1" name="rating" value="1" />
                                                                        <label for="star1" title="Very Poor">1 star</label>
                                                                    </fieldset>
                                                                </div>-->
                                    <div class="lato lightgrey weight-black">{{$course['text']}}</div>
                                </div>
                            </article>
                        @endforeach

                    </div>

                    <!--                <p class="topmargin_60">
                                        <a href="#" class="theme_button inverse">Read more</a>
                                    </p>-->

                </div>
            </div>
        </div>
    </section>

    <section class="ls section_padding_top_150 section_padding_bottom_75">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-center">
                    <h2 class="section_header">{{__('custom.appt1')}}</h2>
                    <p>{{__('custom.appt2')}}</p>
                </div>
            </div>
            <div class="row topmargin_30">
                <div class="col-sm-12">
                    {!! $calendar->calendar() !!}
                </div>
            </div>
        </div>
        <!--<div class="container">
          <div class="row">
              <div class="col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-center">
                  <h2 class="section_header">Make an Appointment</h2>
                  <p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua</p>
              </div>
          </div>
          <div class="row topmargin_30">
              <div class="col-sm-12">


                                <div class="calendar appointment-calendar">
                                      <div class="calendar-header ds">
                                          <div class="calendar-navigation text-center greylinks">
                                              <a href="#" class="prev pull-left">
                                                  Aug
                                              </a>
                                              <h5 class="lato weight-black">September 2016</h5>
                                              <a href="#" class="next pull-right">
                                                  Oct
                                              </a>
                                          </div>
                                      </div>
                                      <div class="week-days">
                                          <div class="sunday">Mon</div>
                                          <div class="monday">Tue</div>
                                          <div class="tuesday">Wed</div>
                                          <div class="wednesday">Thu</div>
                                          <div class="thursday">Fri</div>
                                          <div class="friday">Sat</div>
                                          <div class="saturday">Sun</div>
                                      </div>
                                      <div class="month-box">
                                          <div class="week-row">
                                              <div class="day-cell">
                                                  <span class="day-date">1</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">2</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">3</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">4</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">5</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">6</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">7</span>
                                              </div>
                                          </div>
                                          <div class="week-row">
                                              <div class="day-cell">
                                                  <span class="day-date">8</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">9</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">10</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">11</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">12</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">13</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">14</span>
                                              </div>
                                          </div>
                                          <div class="week-row selected-row">
                                              <div class="day-cell">
                                                  <span class="day-date">15</span>
                                              </div>
                                              <div class="day-cell selected-day">
                                                  <span class="day-date">16</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">17</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">18</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">19</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">20</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">21</span>
                                              </div>
                                          </div>
                                          <div class="week-row">
                                              <div class="day-cell">
                                                  <span class="day-date">22</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">23</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">24</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">25</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">26</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">27</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">28</span>
                                              </div>
                                          </div>
                                          <div class="week-row">
                                              <div class="day-cell">
                                                  <span class="day-date">29</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">30</span>
                                              </div>
                                              <div class="day-cell">
                                                  <span class="day-date">31</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">1</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">2</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">3</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">4</span>
                                              </div>
                                          </div>
                                          <div class="week-row">
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">5</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">6</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">7</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">8</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">9</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">10</span>
                                              </div>
                                              <div class="day-cell not-cur-month">
                                                  <span class="day-date">11</span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

              </div>
          </div>
      </div>-->
    </section>

    <section
            class="ls section_padding_top_75 section_padding_bottom_150 table_section table_section_md columns_margin_bottom_30">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="section_header">{{__('custom.story')}}
                        <br>
                        <!--<strong class="big">of Success</strong>-->
                    </h2>
                    <p class="fontsize_28 highlight thin topmargin_50">{{$stories['title']}}</p>
                    <blockquote class="no-borde                    r">
                        {{$stories['description']}}
                    </blockquote>
                    <!--<a href="#" class="theme_button inverse topmargin_20">More stories</a>-->
                </div>
                <div class="col-md-6 bottommargin_0">
                    <div id="container1" class="twentytwenty-container">
                        @if($stories['images'])
                            @foreach(json_decode($stories['images']) as $img)
                                <img src="{{$img}}" alt=""/>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ls ms section_padding_top_120 section_padding_bottom_100 parallax page_testimonials">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-center">
                    <div class="owl-carousel" data-responsive-lg="1" data-responsive-md="1" data-responsive-sm="1"
                         data-dots="true">
                        @foreach($quotes as $row)
                            <blockquote class="blockquote-item">
                                <div class="item-meta">
                                    <img src="{{asset('storage/'.$row['picture'])}}" alt="">
                                    <h5 class="small-text">{{$row['title']}}</h5>
                                </div>
                                <p>{{$row['description']}}</p>
                            </blockquote>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="ls section_padding_top_150 section_padding_bottom_120 columns_margin_bottom_30">
        <div class="container">
            <div class="row">
                @foreach($top_posts_data as $i => $post)
                    <div class="col-sm-6 col-md-4">
                        <article class="vertical-item content-padding with_shadow text-center">
                            <div class="item-media">
                                <img src="{{$post['picture']}}" alt="">
                            </div>
                            <div class="item-content">
                                <div class="categories-links small-text">
                                    <a href="#"></a>
                                </div>
                                <h4 class="entry-title">
                                    <a href="{{url('blog/'.$post['id'])}}">{{$post['title']}}</a>
                                </h4>
                                {!! substr($post['body'],0,100) !!}
                            </div>
                        </article>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    {{--<section class="ls section_padding_top_150 section_padding_bottom_120 columns_margin_bottom_30">--}}
    {{--<div class="container">--}}
    {{--<div class="row">--}}
    {{--@foreach($packages as $row)--}}
    {{--<div class="col-sm-6 col-md-4">--}}
    {{--<ul class="price-table style1">--}}
    {{--<li class="plan---}}
    {{--name">--}}
    {{--<h3>{{$row['title']}}</h3>--}}
    {{--</li>--}}
    {{--<li class="plan-price">--}}
    {{--<span>{{$row['name']}}</span>--}}
    {{----}}
    {{--</li>--}}
    {{--<li class="features-list">--}}
    {{--<ul class="greylinks">--}}
    {{--<li>--}}
    {{--<a href="#">{{$row['text1']}}</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">{{$row['text2']}}</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">{{$row['text3']}}</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">{{$row['text4']}}</a>--}}
    {{--</li>--}}
    {{--<li>--}}
    {{--<a href="#">{{$row['text5']}}</a>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</li>--}}
    {{--<li class="call-to-action">--}}
    {{--<a href="{{$row['button_link']}}" class="theme_button inverse">{{$row['button_text']}}</a>--}}
    {{--</li>--}}
    {{--</ul>--}}
    {{--</div>--}}
    {{--@endforeach--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
@endsection