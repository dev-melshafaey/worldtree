@extends('index')
@section('content')

    <section class="page_breadcrumbs ds parallax section_padding_75"
             style="background-image: url({{asset(\Illuminate\Support\Facades\Storage::url($bg->path))}})">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="weight-black">{{$posts['title']}}</h1>
                    <ol class="breadcrumb darklinks grey">
                        <li>
                            <a href="{{url('/')}}">
                                {{__('custom.menu.home')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{url('blog')}}">{{__('custom.menu.blog')}}</a>
                        </li>
                        <li class="active">{{$posts['title']}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="ls section_padding_150 columns_padding_30">
        <div class="container">
            <div class="row">

                <div class="col-sm-10 col-sm-push-1">

                    <article class="vertical-item content-padding">

                        @if($posts['video'] != '')
                            <div class="embed-responsive embed-responsive-3by2">
                                <a href="{{$posts['video']}}" class="embed-placeholder">
                                    <img src="{{$posts['picture']}}" alt="{{$posts['title']}}">
                                </a>
                            </div>
                        @else
                            <div class="entry-thumbnail bottommargin_50">
                                <img src="{{$posts['picture']}}" alt="{{$posts['title']}}">
                            </div>
                        @endif

                        <div class="item-content with_shadow">
                            <header class="entry-header">

                                <div class="item-meta small-text">

                                <span class="date">
                                    <time datetime="2016-01-10T15:05:23+00:00" class="entry-date">
                                        {{$posts['post_date']}}
                                    </time>
                                </span>

                                    <span class="categories-links">
                                        {{--<a rel="category" href="blog-full.html">Books</a>--}}
                                        @if($posts['views'])
                                            {{count($posts['views'])}} Views
                                        @endif
                                    </span>

                                </div>
                                <!-- .entry-meta -->

                                <h4 class="entry-title">
                                    <a href="{{url('blog/'.$posts['id'])}}" rel="bookmark">{{$posts['title']}}</a>
                                </h4>

                            </header>
                            {!! $posts['body'] !!}
                            <div class="author-meta side-item topmargin_80 with_background">

                                <div class="row display_table_md">

                                    <div class="col-md-4 display_table_cell_md">
                                        <div class="item-media">
                                            @if($posts['author_picture'])
                                                <img src="{{asset('storage/'.$posts['author_picture'])}}"
                                                     alt="{{$posts['author_name']}}">
                                            @else
                                                <img alt="" src="{{asset('images/faces/04.jpg')}}">
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-8 display_table_cell_md">
                                        <div class="item-content">
                                            <h4>{{$posts['author_name']}}</h4>
                                            <p class="bottommargin_15">
                                                {{$posts['author_description']}}
                                            </p>
                                            <span class="author-social">
                                            <a href="{{$posts['author_facebook']}}"
                                               class="social-icon color-bg-icon rounded-icon soc-facebook"></a>
                                            <a href="{{$posts['author_twitter']}}"
                                               class="social-icon color-bg-icon rounded-icon soc-twitter"></a>
                                            <a href="{{$posts['author_googleplus']}}"
                                               class="social-icon color-bg-icon rounded-icon soc-google"></a>
                                        </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </article>

                    <div class="with_shadow with_padding2 topmargin_80">

                        <div class="star-rating2">
                            <fieldset>
                                <input type="radio" id="star5" name="rating" value="5"/>
                                <label for="star5" title="Outstanding"></label>
                                <input type="radio" id="star4" name="rating" value="4"/>
                                <label for="star4" title="Very Good"></label>
                                <input type="radio" id="star3" name="rating" value="3"/>
                                <label for="star3" title="Good"></label>
                                <input type="radio" id="star2" name="rating" value="2"/>
                                <label for="star2" title="Poor"></label>
                                <input type="radio" id="star1" name="rating" value="1"/>
                                <label for="star1" title="Very Poor"></label>
                            </fieldset>
                        </div>

                        {{--<h3 class="module-header">{{__('custom.lcomment')}}</h3>--}}

                        <div class="comment-respond" id="respond">
                            <form class="" id="" method="post">
                                <div class="row columns_margin_0 columns_padding_5">
                                <!--                                <div class="col-md-4">
                                                                    <p class="comment-form-author">
                                                                        <label for="author">{{__('generic.name')}}
                                        <span class="required">*</span>
                                    </label>

                                    <input type="text" aria-required="true" size="30" value="" name="author" id="author" class="form-control" placeholder="Full Name">
                                </p>
                            </div>
                            <div class="col-md-4">
                                <p class="comment-form-email">
                                    <label for="comment_email">{{__('generic.comment')}}
                                        <span class="required">*</span>
                                    </label>

                                    <input type="email" aria-required="true" size="30" value="" name="comment_email" id="comment_email" class="form-control" placeholder="Emain Address">
                                </p>
                            </div>
                            <div class="col-md-4">
                                <p class="comment-form-phone">
                                    <label for="comment_phone">Phone
                                        <span class="required">*</span>
                                    </label>

                                    <input type="text" size="30" value="" name="comment_phone" id="comment_phone" class="form-control" placeholder="Phone Number">
                                </p>
                            </div>-->
                                    <div class="col-md-12">
                                        <p class="comment-form-chat">
                                            <label for="comment">{{__('custom.lcomment')}}</label>
                                            <!-- <i class="rt-icon2-pencil3"></i> -->
                                            <textarea aria-required="true" rows="10" cols="45" id="comment"
                                                      class="form-control"
                                                      placeholder="{{__('custom.comment')}}..."></textarea>
                                        </p>
                                    </div>
                                </div>
                                <p class="form-submit">
                                    <button type="button" onclick="addComment({{$posts['id']}})" id=""
                                            class="theme_button color1">{{__('generic.submit')}}</button>
                                </p>
                            </form>
                        </div>
                        <!-- #respond -->

                        <div class="comments-area" id="comments">
                            <ol id="comment-list" class="comment-list">
                                <!--                            <li class="comment even thread-even depth-1 parent">
                                                                <article class="comment-body media">
                                                                    <div class="media-left">
                                                                        <img class="media-object" alt="" src="images/faces/05.jpg">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span class="reply greylinks">
                                                                            <a href="#respond">
                                                                                <i class="fa fa-reply"></i>
                                                                            </a>
                                                                        </span>
                                                                        <div class="comment-meta">
                                                                            <h4 class="entry-title">
                                                                                <a class="author_url" rel="external nofollow" href="#">Derrick Terry</a>
                                                                            </h4>
                                                                            <span class="comment-date small-text">
                                                                                <time datetime="2016-11-08T15:05:23+00:00" class="entry-date">September 13, 2016</time>
                                                                            </span>
                                                                        </div>
                                                                        <p>Capicola jerky beef ribs porchetta meatloaf bresaola ribeye bacon pork boudin flank salami tenderloin picanha sirloin.</p>
                                                                    </div>
                                                                </article>


                                                                <ol class="children">
                                                                    <li class="comment byuser odd alt depth-2 parent">
                                                                        <article class="comment-body media">
                                                                            <div class="media-left">
                                                                                <img class="media-object" alt="" src="images/faces/03.jpg">
                                                                            </div>
                                                                            <div class="media-body">
                                                                                <span class="reply greylinks">
                                                                                    <a href="#respond">
                                                                                        <i class="fa fa-reply"></i>
                                                                                    </a>
                                                                                </span>
                                                                                <div class="comment-meta">
                                                                                    <h4 class="entry-title">
                                                                                        <a class="author_url" rel="external nofollow" href="#">Marie Mendoza</a>
                                                                                    </h4>
                                                                                    <span class="comment-date small-text">
                                                                                        <time datetime="2016-11-08T15:05:23+00:00" class="entry-date">September 12, 2016</time>
                                                                                    </span>
                                                                                </div>
                                                                                <p>Capicola jerky beef ribs porchetta meatloaf bresaola ribeye bacon pork boudin flank salami tenderloin picanha sirloin.</p>
                                                                            </div>
                                                                        </article>
                                                                        .comment-body

                                                                        <ol class="children">
                                                                            <li class="comment byuser even depth-3">
                                                                                <article class="comment-body media">
                                                                                    <div class="media-left">
                                                                                        <img class="media-object" alt="" src="images/faces/01.jpg">
                                                                                    </div>
                                                                                    <div class="media-body">
                                                                                        <span class="reply greylinks">
                                                                                            <a href="#respond">
                                                                                                <i class="fa fa-reply"></i>
                                                                                            </a>
                                                                                        </span>
                                                                                        <div class="comment-meta">
                                                                                            <h4 class="entry-title">
                                                                                                <a class="author_url" rel="external nofollow" href="#">Johnny Garcia</a>
                                                                                            </h4>
                                                                                            <span class="comment-date small-text">
                                                                                                <time datetime="2016-11-08T15:05:23+00:00" class="entry-date">September 10, 2016</time>
                                                                                            </span>
                                                                                        </div>
                                                                                        <p>Capicola jerky beef ribs porchetta meatloaf bresaola ribeye bacon pork boudin flank salami tenderloin</p>
                                                                                    </div>
                                                                                </article>
                                                                                .comment-body
                                                                            </li>
                                                                            #comment-##
                                                                        </ol>
                                                                        .children
                                                                    </li>
                                                                    #comment-##
                                                                </ol>

                                                            </li>-->
                                <!--                            <li class="comment byuser odd alt thread-odd thread-alt depth-1">
                                                                <article class="comment-body media">
                                                                    <div class="media-left">
                                                                        <img class="media-object" alt="" src="images/faces/02.jpg">
                                                                    </div>
                                                                    <div class="media-body">
                                                                        <span class="reply greylinks">
                                                                            <a href="#respond">
                                                                                <i class="fa fa-reply"></i>
                                                                            </a>
                                                                        </span>
                                                                        <div class="comment-meta">
                                                                            <h4 class="entry-title">
                                                                                <a class="author_url" rel="external nofollow" href="#">Alan Smith</a>
                                                                            </h4>
                                                                            <span class="comment-date small-text">
                                                                                <time datetime="2016-11-08T15:05:23+00:00" class="entry-date">September 12, 2016</time>
                                                                            </span>
                                                                        </div>
                                                                        <p>First Level Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis.</p>
                                                                    </div>
                                                                </article>
                                                                .comment-body
                                                            </li>-->

                                @if($posts['comments'])
                                    @foreach($posts['comments'] as $comment)
                                        <li id="li_{{$comment['id']}}" class="comment byuser even thread-even depth-1">
                                            <article class="comment-body media">
                                                <div class="media-left">
                                                    @if($comment['user']['avatar'])
                                                        <img class="media-object" alt=""
                                                             src="{{\Illuminate\Support\Facades\Storage::url($comment['user']['avatar'])}}">
                                                    @else
                                                        <img class="media-object" alt=""
                                                             src="{{asset('images/faces/04.jpg')}}">
                                                    @endif
                                                </div>
                                                <div class="media-body">
                                                    {{--<span class="reply greylinks">--}}
                                                    {{--<a href="#respond">--}}
                                                    {{--<i class="fa fa-reply"></i>--}}
                                                    {{--</a>--}}
                                                    {{--</span>--}}
                                                    <div class="comment-meta">
                                                        <h4 class="entry-title">
                                                            <a class="author_url" rel="external nofollow"
                                                               href="#">{{$comment['user']['name']}}</a>
                                                        </h4>
                                                        <span class="comment-date small-text">
                                                            <time datetime="2016-11-08T15:05:23+00:00"
                                                                  class="entry-date">{{\Carbon\Carbon::parse($comment['created_at'])->format('d F Y - H:i')}}</time>
                                                        </span>
                                                    </div>

                                                    <div class="row greylinks">
                                                        @if(auth()->user() && auth()->id() == $comment['user']['id'])
                                                            <a title="Edit" href="javascript:void(0)"
                                                               onclick="editComment({{$comment['id']}})">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                        @endif

                                                        @if(auth()->user() && (auth()->user()->role->name == 'admin' || auth()->user()->role->name == 'Moderator'))
                                                            <a title="Delete" href="javascript:void(0)"
                                                               onclick="deleteComment({{$comment['id']}})">
                                                                <i class="fa fa-remove"></i>
                                                            </a>
                                                        @endif

                                                        <p id="commentp_{{$comment['id']}}"
                                                           class="bottommargin_0">{{$comment['comment']}}</p>
                                                    </div>

                                                    <div class="row hidden" id="edit-box_{{$comment['id']}}">
                                                        <input class="col-md-11 comment_box" type="text"
                                                               id="comment_{{$comment['id']}}"
                                                               style="height: 40px;" value="{{$comment['comment']}}"/>

                                                        <div class="col-md-1 greylinks">
                                                            <a title="Save" class="saveComment pull-right"
                                                               id="comment_{{$comment['id']}}_save"
                                                               href="javascript:void(0)"
                                                               onclick="saveComment({{$comment['id']}})">
                                                                <i class="glyphicon glyphicon-floppy-save"></i>
                                                            </a>
                                                            &nbsp;
                                                            &nbsp;
                                                            <a title="Cancel" class="cancelComment pull-right"
                                                               href="javascript:void(0)"
                                                               onclick="cancelComment({{$comment['id']}})">
                                                                <i class="glyphicon glyphicon-remove"></i>
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </article>

                                        </li>

                                    @endforeach
                                @endif

                            </ol>

                        </div>
                        <!-- #comments -->

                    </div>

                    <div class="row topmargin_80 columns_padding_5">
                        <div class="col-sm-6">
                            <div class="vertical-item blog-button blog-prev text-center ds ms2 before_cover">
                                <div class="media-links">
                                    <a href="@if($prev) {{url('blog/'.$prev['id'])}} @else # @endif"
                                       class="abs-link"></a>
                                </div>

                                <div class="display_table">
                                    <div class="display_table_cell">
                                        <div class="small-text highlight">{{__('custom.prev')}}</div>
                                        <h4 class="entry-title margin_0">@if($prev) {{$prev['title']}} @else {{$posts['title']}} @endif</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="vertical-item blog-button blog-next text-center ds ms2 before_cover">
                                <div class="media-links">
                                    <a href="@if($next) {{url('blog/'.$next['id'])}} @else # @endif"
                                       class="abs-link"></a>
                                </div>

                                <div class="display_table">
                                    <div class="display_table_cell">
                                        <div class="small-text highlight">{{__('custom.next')}}</div>
                                        <h4 class="entry-title margin_0">@if($next) {{$next['title']}} @else {{$posts['title']}} @endif</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!--eof .col-sm-8 (main content)-->

            </div>
        </div>
    </section>
@endsection