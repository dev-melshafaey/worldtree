@extends('index')
@section('content')

<section class="page_breadcrumbs ds parallax section_padding_75" style="background-image: url({{asset(\Illuminate\Support\Facades\Storage::url($bg->path))}})">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1 class="weight-black"> {{__('custom.menu.about')}}</h1>
                <ol class="breadcrumb darklinks grey">
                    <li>
                        <a href="/">
                            {{__('custom.menu.home')}}
                        </a>
                    </li>

                    <li class="active">{{__('custom.menu.about')}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>

<section class="ls columns_padding_bottom section_padding_top_50 table_section table_section_md">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h3 class="bottommargin_40">
                    <strong class="highlight weight-black fontsize_76"></strong> {{$about['title']}}
                </h3>
                <p>
                    {{$about['text']}}
                </p>
                <!--<a href="about.html" class="theme_button inverse topmargin_30">About me</a>-->
                <p class="lato grey fontsize_20 text-uppercase topmargin_40">
                    {{__('custom.call')}}
                    <br>
                    <span class="fontsize_38 weight-black">{{$data[0]['mobile']}}</span>
                </p>
            </div>
            <div class="col-md-6 text-center bottommargin_0">
                <img src="{{asset('storage/'.$about['picture'])}}" alt="">
            </div>
        </div>
    </div>
</section>

<section class="ls ms section_padding_top_120 section_padding_bottom_100 parallax page_testimonials">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8 text-center">
                <div class="owl-carousel" data-responsive-lg="1" data-responsive-md="1" data-responsive-sm="1" data-dots="true">
                    @foreach($quotes as $row)
                    <blockquote class="blockquote-item">
                        <div class="item-meta">
                            <img src="{{asset('storage/'.$row['picture'])}}" alt="">
                            <h5 class="small-text">{{$row['title']}}</h5>
                        </div>
                        <p>{{$row['description']}}</p>
                    </blockquote>
                   @endforeach

                </div>
            </div>
        </div>
    </div>
</section>

<section class="ls section_padding_top_150 section_padding_bottom_120 columns_margin_bottom_30">
    <div class="container">
        <div class="row">
            @foreach($points as $point)
            <div class="col-sm-6 col-md-3">
                <div class="teaser text-center">
                    <a class="teaser_icon size_small big border_icon round" href="#">
                        <i class="fa {{$point['icon']}} highlight"></i>
                    </a>
                    <h4 class="text-uppercase">
                        <a href="#">{{explode(' ',$point['title'])[0]}}
                            <br>
                            <strong class="weight-black">
                                @if(count(explode(' ',$point['title'])) > 1)
                                {{explode(' ',$point['title'])[1]}}
                                @endif
                            </strong>
                        </a>
                    </h4>
                    <p>
                        {{$point['description']}}
                    </p>
                </div>
            </div>
            @endforeach
           
        </div>
    </div>
</section>

@endsection