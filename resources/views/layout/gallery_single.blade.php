@extends('index')
@section('content')

<section class="page_breadcrumbs ds parallax section_padding_75" style="background-image: url({{asset(\Illuminate\Support\Facades\Storage::url($bg->path))}})">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <h1 class="weight-black">{{$img['title']}}</h1>
                <ol class="breadcrumb darklinks grey">
                    <li>
                        <a href="{{url('/')}}">
                            {{__('custom.menu.home')}}
                        </a>
                    </li>
                    <li><a href="{{url('gallery')}}"> {{__('custom.menu.gallery')}}</a></li>
                    <li class="active">{{$img['title']}}</li>
                </ol>
            </div>
        </div>
    </div>
</section>


<section class="ls section_padding_top_75 section_padding_bottom_100">
    <div class="container">
        <div class="row">

            <div class="col-md-10 col-md-push-1 text-center">

                <h1 class="section_header">
                    {{$img['title']}}
                </h1>

                <!--                <div class="categories-links small-text bottommargin_40">
                                    <a rel="category" href="blog-right.html">Category 1</a>
                                    <a rel="category" href="blog-right.html">Category 2</a>
                                </div>-->

                <div class="vertical-item gallery-extended-item content-padding with_shadow bottom-color-border">
                    <div class="item-media">
                        <img src="{{$img['picture']}}" alt="" />
                    </div>
                    <div class="item-content text-center">

                        {!! $img['text'] !!}

                        <div class="share-gallery-single">
                            @if($img['facebook'])<a target="_blank" href="{{$img['facebook']}}" class="social-icon color-bg-icon soc-facebook"></a>@endif
                            @if($img['twitter'])<a target="_blank" href="{{$img['twitter']}}" class="social-icon color-bg-icon soc-twitter"></a>@endif
                            @if($img['googleplus'])<a target="_blank" href="{{$img['googleplus']}}" class="social-icon color-bg-icon soc-google"></a>@endif
                            @if($img['linkedin'])<a target="_blank" href="{{$img['linkedin']}}" class="social-icon color-bg-icon soc-linkedin"></a>@endif
                            @if($img['pinterest'])<a target="_blank" href="{{$img['pinterest']}}" class="social-icon color-bg-icon soc-pinterest"></a>@endif

                        </div>

                    </div>
                </div>


            </div>

        </div>
    </div>
</section>

<section class="ls section_padding_100">
<p class="fontsize_28 highlight thin topmargin_50 leftmargin_50">{{__('custom.more_gallery')}}</p>
    <div class="owl-carousel related-photos-carousel" data-margin="0" data-nav="true" data-loop="true" data-items="3">
    
        @foreach($images as $image)
        <div>
            <div class="vertical-item gallery-title-item gallery-item content-absolute">
                <div class="item-media">
                <a href="{{url('gallery/'.$image['id'])}}"><img src="{{$image['picture']}}" alt="" /></a>
                    <!-- <img src="{{asset('storage/'.$image['picture'])}}" alt="" /> -->
                    <!-- <div class="media-links"> -->
                        <!-- <a class="abs-link prettyPhoto" title="" data-gal="prettyPhoto[gal]" href="{{asset('storage/'.$image['picture'])}}"></a> -->
                    <!-- </div> -->
                </div>
            </div>
            <div class="item-title text-center">
                <span class="categories-links small-text">
                    <a rel="category" href="#">{{$image['category']['name']}}</a>
                </span>
                <h3>
                    <a href="{{url('gallery/'.$image['id'])}}">{{$image['title']}}</a>
                </h3>

            </div>
        </div>

        @endforeach

    </div>


</section>


@endsection   