@extends('index')
@section('content')
    <section class="page_breadcrumbs ds parallax section_padding_75"
             style="background-image: url({{asset(\Illuminate\Support\Facades\Storage::url($bg->path))}})">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h1 class="weight-black">{{__('custom.menu.timetable')}}</h1>
                    <ol class="breadcrumb darklinks grey">
                        <li>
                            <a href="{{url('/')}}">
                                {{__('custom.menu.home')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{url('gallery')}}">
                                {{__('custom.menu.gallery')}}
                            </a>
                        </li>
                        <li class="active">{{__('custom.menu.timetable')}}</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section class="ls section_padding_top_100 section_padding_bottom_75">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <h2 class="section_header small text-center">
                        <strong>{{__('timetable.header')}}</strong>
                    </h2>
                </div>
            </div>

            {{--            <div class="row">--}}
            {{--                <div class="text-center filters col-sm-12">--}}
            {{--                    <ul id="timetable_filter">--}}
            {{--                        <li>--}}
            {{--                            <a data-filter="all" href="#" class="selected">All</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a data-filter=".books" href="#">books</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a data-filter=".coaching" href="#">coaching</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a data-filter=".presentations" href="#">presentations</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a data-filter=".meetings" href="#">meetings</a>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <a data-filter=".consultations" href="#">consultations</a>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}
            {{--                    <div class="clearfix"></div>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            <div class="row">
                <div class="col-sm-12">
                    <div class="table-responsive">
                        <table class="table_template darklinks" id="timetable">
                            <thead>
                            <tr>
                                <th></th>
                                <th>{{__('days.Monday')}}</th>
                                <th>{{__('days.Tuesday')}}</th>
                                <th>{{__('days.Wednesday')}}</th>
                                <th>{{__('days.Thursday')}}</th>
                                <th>{{__('days.Friday')}}</th>
                                <th>{{__('days.Saturday')}}</th>
                                <th>{{__('days.Sunday')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                           
                                <tr>
                                    <th>8:00 - 9:00</th>
                                    <td>
                                        <a href="#" class="books">
{{--                                            @if($time['time'] == '8-9' & $time['day'] == 'monday')--}}
                                                {{$time['8-9']['monday'] ?? ''}}
{{--                                            @endif--}}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['8-9']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['8-9']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['8-9']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['8-9']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['8-9']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['8-9']['sunday'] ?? '' }}
                                        </a>
                                    </td>
                                </tr>
                           

                           
                                <tr>
                                    <th>9:00 - 10:00</th>
                                    <td>
                                        <a href="#" class="coaching">
                                            {{$time['9-10']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="coaching">
                                            {{$time['9-10']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['9-10']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['9-10']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['9-10']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['9-10']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['9-10']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>
                           

                           
                                <tr>
                                    <th>10:00 - 11:00</th>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['10-11']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['10-11']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['10-11']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['10-11']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['10-11']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['10-11']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['10-11']['sunday'] ?? '' }}
                                        </a>
                                    </td>
                                </tr>
                           

                           
                                <tr>
                                    <th>11:00 - 12:00</th>
                                    <td>
                                        <a href="#" class="coaching">
                                            {{$time['11-12']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="coaching">
                                            {{$time['11-12']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['11-12']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="coaching">
                                            {{$time['11-12']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="coaching">
                                            {{$time['11-12']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['11-12']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['11-12']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>

                           
                                <tr>
                                    <th>12:00 - 13:00</th>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['12-13']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['12-13']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['12-13']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['12-13']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['12-13']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['12-13']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['12-13']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>

                           
                                <tr>
                                    <th>13:00 - 14:00</th>
                                    <td>
                                        <a href="#" class="coaching">
                                            {{$time['13-14']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['13-14']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['13-14']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['13-14']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['13-14']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="coaching">
                                            {{$time['13-14']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['13-14']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>
                           


                           
                                <tr>
                                    <th>14:00 - 15:00</th>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['14-15']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['14-15']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['14-15']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['14-15']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['14-15']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['14-15']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['14-15']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>
                           

                                <tr>
                                    <th>15:00 - 16:00</th>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['15-16']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['15-16']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="coaching">
                                            {{$time['15-16']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['15-16']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['15-16']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['15-16']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['15-16']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>
                           


                           
                                <tr>
                                    <th>16:00 - 17:00</th>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['16-17']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['16-17']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['16-17']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['16-17']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['16-17']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['16-17']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['16-17']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>
                           

                           
                                <tr>
                                    <th>17:00 - 18:00</th>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['17-18']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['17-18']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['17-18']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['17-18']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['17-18']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['17-18']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['17-18']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>

                           
                                <tr>
                                    <th>18:00 - 19:00</th>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['18-19']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="books">
                                            {{$time['18-19']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['18-19']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['18-19']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['18-19']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['18-19']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['18-19']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>
                           

                                <tr>
                                    <th>19:00 - 20:00</th>
                                    <td>
                                        <a href="#" class="presentations">
                                            {{$time['19-20']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                           {{$time['19-20']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                             {{$time['19-20']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                             {{$time['19-20']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                             {{$time['19-20']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                             {{$time['19-20']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="presentations">
                                             {{$time['19-20']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>
                           

                           
                                <tr>
                                    <th>20:00 - 21:00</th>
                                    <td>
                                        <a href="#" class="meetings">
                                             {{$time['20-21']['monday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                             {{$time['20-21']['tuesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                           {{$time['20-21']['wednesday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="consultations">
                                            {{$time['20-21']['thursday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['20-21']['friday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['20-21']['saturday'] ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        <a href="#" class="meetings">
                                            {{$time['20-21']['sunday'] ?? '' }}
                                        </a>
                                    </td>

                                </tr>
                           

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

