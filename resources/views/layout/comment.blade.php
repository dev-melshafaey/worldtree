<li id="li_{{$comment->id}}" class="comment byuser even thread-even depth-1">
    <article class="comment-body media">
        <div class="media-left">
            @if(auth()->user()->avatar)
                <img class="media-object" alt=""
                     src="{{\Illuminate\Support\Facades\Storage::url(auth()->user()->avatar)}}">
            @else
                <img class="media-object" alt=""
                     src="{{asset('images/faces/04.jpg')}}">
            @endif
        </div>
        <div class="media-body">
            <span class="reply greylinks">
                <a href="#respond">
                    <i class="fa fa-reply"></i>
                </a>
            </span>
            <div class="comment-meta">
                <h4 class="entry-title">
                    <a class="author_url" rel="external nofollow" href="#">{{$name}}</a>
                </h4>
                <span class="comment-date small-text">
                    <time datetime="2016-11-08T15:05:23+00:00"
                          class="entry-date">{{\Carbon\Carbon::parse($date)->format('d F Y - H:i')}}</time>
                </span>
            </div>
            <div class="row greylinks">
               {{--@if(auth()->user() && auth()->id() == $comment->user->id)--}}
                    <a title="Edit" href="javascript:void(0)"
                       onclick="editComment({{$comment->id}})">
                        <i class="fa fa-pencil"></i>
                    </a>
                {{--@endif--}}
                @if(auth()->user()->role->name == 'admin' || auth()->user()->role->name == 'Moderator')
                    <a title="Delete" href="javascript:void(0)"
                       onclick="deleteComment({{$comment->id}})">
                        <i class="fa fa-remove"></i>
                    </a>
                @endif

                <p id="commentp_{{$comment->id}}" class="bottommargin_0">{{$comment->comment}}</p>
            </div>

            <div class="row hidden" id="edit-box_{{$comment->id}}">
                <input class="col-md-11 comment_box" type="text"
                       id="comment_{{$comment->id}}"
                       style="height: 40px;" value="{{$comment->comment}}"/>

                <div class="col-md-1 greylinks">
                    <a title="Save" class="saveComment pull-right"
                       id="comment_{{$comment->id}}_save" href="javascript:void(0)"
                       onclick="saveComment({{$comment->id}})">
                        <i class="glyphicon glyphicon-floppy-save"></i>
                    </a>
                    &nbsp;
                    &nbsp;
                    <a title="Cancel" class="cancelComment pull-right"
                       href="javascript:void(0)"
                       onclick="cancelComment({{$comment->id}})">
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                </div>
            </div>
        </div>
    </article>
</li>