@extends('index')
@section('content')

    <section class="ls page_not_found"  style="background-image: url({{asset('images/404.jpg')}})">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-6 col-sm-6">
                    <h2>Oops, page not found!</h2>
{{--                    <p>--}}
{{--                        You can search what interested:--}}
{{--                    </p>--}}
{{--                    <div class="widget widget_search">--}}
{{--                        <form class="signup form-inline" action="./" method="get">--}}
{{--                            <div class="form-group">--}}
{{--                                <input name="email" type="email" class="mailchimp_email form-control"--}}
{{--                                       placeholder="Your Email Address">--}}
{{--                            </div>--}}
{{--                            <button type="submit" class="theme_button">--}}
{{--                                <span class="sr-only">Send</span>--}}
{{--                                <i class="fa fa-pencil highlight"></i>--}}
{{--                            </button>--}}
{{--                            <div class="response"></div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
                    <p class="topmargin_40">
                        <a href="{{route('home')}}" class="theme_button color1">Go Home</a>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="page_copyright ls section_padding_15">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <p class="lato small-text">&copy; copyright 2016 all rights reserved</p>
                </div>
            </div>
        </div>
    </section>
@endsection