<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="{{ app()->getLocale() }}" class="no-js">
    <!--<![endif]-->

    <head>
        <title>World Tree | Give To Live</title>
        <meta charset="utf-8">
        <!--[if IE]>
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <![endif]-->
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="icon" href="{{ URL::asset('images/favicon.ico')}}" >

        <link rel="stylesheet" href="{{ URL::asset(config('config.ST_LNG').'/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.css" />
        <link rel="stylesheet" href="{{ URL::asset(config('config.ST_LNG').'/css/main.css')}}" id="color-switcher-link">

        <link rel="stylesheet" href="{{ URL::asset(config('config.ST_LNG').'/css/animations.css')}}">
        <link rel="stylesheet" href="{{ URL::asset(config('config.ST_LNG').'/css/fonts.css')}}">
        <script src="{{ URL::asset(config('config.ST_LNG').'/js/vendor/modernizr-2.6.2.min.js')}}"></script>

        <!--[if lt IE 9]>
                <script src="{{ URL::asset(config('config.ST_LNG').'/js/vendor/html5shiv.min.js')}}"></script>
                <script src="{{ URL::asset(config('config.ST_LNG').'/js/vendor/respond.min.js')}}"></script>
                <script src="{{ URL::asset(config('config.ST_LNG').'/js/vendor/jquery-1.12.4.min.js')}}"></script>
        <![endif]-->

    </head>

    <body>
        <!--[if lt IE 9]>
                <div class="bg-danger text-center">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/" class="highlight">upgrade your browser</a> to improve your experience.</div>
        <![endif]-->

        <div class="preloader">
            <div class="preloader_image"></div>
        </div>


        <!-- search modal -->
        <div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal">
            <div class="widget widget_search">
                <form method="get" class="searchform form-inline" action="/">
                    <div class="form-group">
                        <input type="text" value="" name="search" class="form-control" placeholder="Search keyword" id="modal-search-input">
                    </div>
                    <button type="submit" class="theme_button">
                        <span class="sr-only">Search</span>
                        <i class="fa fa-search highlight"></i>
                    </button>
                </form>
            </div>
        </div>

        <!-- wrappers for visual page editor and boxed version of template -->
        <div id="canvas">
            <div id="box_wrapper">
                @if(isset($header))
                @include('layout.index_header')   
                @else
                @include('layout.header')    
                @endif

                @yield('content')                                       
                @include('layout.footer')                                       
            </div>
            <!-- eof #box_wrapper -->
        </div>
        <!-- eof #canvas -->

        <script src="{{ URL::asset(config('config.ST_LNG').'/js/compressed.js')}}"></script>
        <script src="{{ URL::asset(config('config.ST_LNG').'/js/main.js')}}"></script>
        <script src="{{ URL::asset('js/scripts.js')}}"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>


        @if(isset($calendar))
        {!! $calendar->script() !!}
        @endif
    </body>

</html>