/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.30-MariaDB : Database - worldtree
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `about` */

DROP TABLE IF EXISTS `about`;

CREATE TABLE `about` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `text` text COLLATE utf8_unicode_ci,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `about` */

insert  into `about`(`id`,`text`,`picture`,`title`) values (1,'من نحن','about\\April2018\\lwinvSkOsgpGr9aINZKx.png','وورلد تري');

/*Table structure for table `about_points` */

DROP TABLE IF EXISTS `about_points`;

CREATE TABLE `about_points` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'fa-star-o',
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `about_points` */

insert  into `about_points`(`id`,`icon`,`title`,`description`,`created_at`,`updated_at`) values (1,'fa-smile-o','كن سعيدا','تيست','2018-04-30 20:06:43','2018-04-30 20:16:55'),(2,'fa-star-o','تيست','تيست','2018-04-30 20:08:04','2018-04-30 20:08:04'),(3,'fa-scissors','تيست','تيست','2018-04-30 20:09:50','2018-04-30 20:09:50'),(4,'fa-codepen','تيست','تيست','2018-04-30 20:11:03','2018-04-30 20:11:03');

/*Table structure for table `articles` */

DROP TABLE IF EXISTS `articles`;

CREATE TABLE `articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci,
  `post_date` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `author_googleplus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `articles` */

insert  into `articles`(`id`,`picture`,`title`,`body`,`post_date`,`author_name`,`author_picture`,`author_description`,`author_facebook`,`author_twitter`,`author_googleplus`,`video`,`created_at`,`updated_at`,`user_id`) values (1,'articles\\May2018\\eY7sa83mz2otrGEPFaG0.jpg','Kielbasa tri-tip officia turducken alcatra filet mignon sint pork','<p>Tri-tip boudin enim turducken prosciutto shank ground round fugiat cow anim capicola veniam pariatur ham. Cillum culpa magna sausage filet mignon. Pork loin enim pancetta bacon, leberkas laborum sunt ullamco id flank shankle aliqua. Irure consequat labore ham. Occaecat labore alcatra sed, ea corned beef ham hock elit fugiat swine irure voluptate sint. Meat exercitation strip steak anim, leberkas pancetta sirloin shankle biltong ullamco kielbasa brisket excepteur. Ad cupim cupidatat fugiat frankfurter tongue. Picanha pork pancetta, bacon sirloin chicken beef fatback burgdoggen filet mignon kevin drumstick leberkas pastrami doner. Shoulder strip steak landjaeger beef ribs venison ribeye. Capicola flank ground round burgdoggen leberkas tri-tip.</p>','2018-05-09','Calvin Richards','articles\\May2018\\lKEnod3IrfgHopT1EgpC.jpg','Leberkas meatloaf alcatra turducken jerky landjaeger t-bone shankle pork chop pork shoulder.',NULL,NULL,NULL,NULL,'2018-05-02 18:45:41','2018-05-02 18:52:37',1),(4,'articles\\May2018\\eY7sa83mz2otrGEPFaG0.jpg','Kielbasa tri-tip officia turducken alcatra filet mignon sint pork','<p>Tri-tip boudin enim turducken prosciutto shank ground round fugiat cow anim capicola veniam pariatur ham. Cillum culpa magna sausage filet mignon. Pork loin enim pancetta bacon, leberkas laborum sunt ullamco id flank shankle aliqua. Irure consequat labore ham. Occaecat labore alcatra sed, ea corned beef ham hock elit fugiat swine irure voluptate sint. Meat exercitation strip steak anim, leberkas pancetta sirloin shankle biltong ullamco kielbasa brisket excepteur. Ad cupim cupidatat fugiat frankfurter tongue. Picanha pork pancetta, bacon sirloin chicken beef fatback burgdoggen filet mignon kevin drumstick leberkas pastrami doner. Shoulder strip steak landjaeger beef ribs venison ribeye. Capicola flank ground round burgdoggen leberkas tri-tip.</p>','2018-05-09','Calvin Richards','articles\\May2018\\lKEnod3IrfgHopT1EgpC.jpg','Leberkas meatloaf alcatra turducken jerky landjaeger t-bone shankle pork chop pork shoulder.',NULL,NULL,NULL,NULL,'2018-05-02 18:45:41','2018-05-02 18:52:37',1),(5,'articles\\May2018\\eY7sa83mz2otrGEPFaG0.jpg','Kielbasa tri-tip officia turducken alcatra filet mignon sint pork','<p>Tri-tip boudin enim turducken prosciutto shank ground round fugiat cow anim capicola veniam pariatur ham. Cillum culpa magna sausage filet mignon. Pork loin enim pancetta bacon, leberkas laborum sunt ullamco id flank shankle aliqua. Irure consequat labore ham. Occaecat labore alcatra sed, ea corned beef ham hock elit fugiat swine irure voluptate sint. Meat exercitation strip steak anim, leberkas pancetta sirloin shankle biltong ullamco kielbasa brisket excepteur. Ad cupim cupidatat fugiat frankfurter tongue. Picanha pork pancetta, bacon sirloin chicken beef fatback burgdoggen filet mignon kevin drumstick leberkas pastrami doner. Shoulder strip steak landjaeger beef ribs venison ribeye. Capicola flank ground round burgdoggen leberkas tri-tip.</p>','2018-05-09','Calvin Richards','articles\\May2018\\lKEnod3IrfgHopT1EgpC.jpg','Leberkas meatloaf alcatra turducken jerky landjaeger t-bone shankle pork chop pork shoulder.',NULL,NULL,NULL,NULL,'2018-05-02 18:45:41','2018-05-02 18:52:37',2);

/*Table structure for table `blog_slides` */

DROP TABLE IF EXISTS `blog_slides`;

CREATE TABLE `blog_slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `blog_slides` */

insert  into `blog_slides`(`id`,`title1`,`title2`,`picture`,`created_at`,`updated_at`) values (1,'Discover','Secret of life','blog-slides\\May2018\\DU5XCdlBSLEfascXIPXm.jpg','2018-05-02 17:30:01','2018-05-02 17:30:01'),(2,'Discover','Secret of life','blog-slides\\May2018\\DU5XCdlBSLEfascXIPXm.jpg','2018-05-02 17:30:01','2018-05-02 17:30:01'),(3,'Discover','Secret of life','blog-slides\\May2018\\DU5XCdlBSLEfascXIPXm.jpg','2018-05-02 17:30:01','2018-05-02 17:30:01');

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`parent_id`,`order`,`name`,`slug`,`created_at`,`updated_at`) values (1,NULL,1,'Category 1','category-1','2018-04-28 11:03:42','2018-04-28 11:03:42'),(2,NULL,1,'Category 2','category-2','2018-04-28 11:03:42','2018-04-28 11:03:42');

/*Table structure for table `courses` */

DROP TABLE IF EXISTS `courses`;

CREATE TABLE `courses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `courses` */

insert  into `courses`(`id`,`picture`,`title`,`user`,`text`,`created_at`,`updated_at`) values (1,'courses\\May2018\\uu960rPi80XSgaerECAW.jpg','test','test','test','2018-05-08 19:56:17','2018-05-08 19:56:17');

/*Table structure for table `data_rows` */

DROP TABLE IF EXISTS `data_rows`;

CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `data_rows` */

insert  into `data_rows`(`id`,`data_type_id`,`field`,`type`,`display_name`,`required`,`browse`,`read`,`edit`,`add`,`delete`,`details`,`order`) values (1,1,'id','number','ID',1,0,0,0,0,0,'',1),(2,1,'name','text','Name',1,1,1,1,1,1,'',2),(3,1,'email','text','Email',1,1,1,1,1,1,'',3),(4,1,'password','password','Password',1,0,0,1,1,0,'',4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,'',5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,'',6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,'',8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'locale','text','Locale',0,1,1,1,1,0,'',12),(12,2,'id','number','ID',1,0,0,0,0,0,'',1),(13,2,'name','text','Name',1,1,1,1,1,1,'',2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),(16,3,'id','number','ID',1,0,0,0,0,0,'',1),(17,3,'name','text','Name',1,1,1,1,1,1,'',2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,'',5),(21,1,'role_id','text','Role',1,1,1,1,1,1,'',9),(22,4,'id','number','ID',1,0,0,0,0,0,'',1),(23,4,'parent_id','select_dropdown','Parent',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(24,4,'order','text','Order',1,1,1,1,1,1,'{\"default\":1}',3),(25,4,'name','text','Name',1,1,1,1,1,1,'',4),(26,4,'slug','text','Slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(27,4,'created_at','timestamp','Created At',0,0,1,0,0,0,'',6),(28,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',7),(29,5,'id','number','ID',1,0,0,0,0,0,'',1),(30,5,'author_id','text','Author',1,0,1,1,0,1,'',2),(31,5,'category_id','text','Category',1,0,1,1,1,0,'',3),(32,5,'title','text','Title',1,1,1,1,1,1,'',4),(33,5,'excerpt','text_area','Excerpt',1,0,1,1,1,1,'',5),(34,5,'body','rich_text_box','Body',1,0,1,1,1,1,'',6),(35,5,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(36,5,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}',8),(37,5,'meta_description','text_area','Meta Description',1,0,1,1,1,1,'',9),(38,5,'meta_keywords','text_area','Meta Keywords',1,0,1,1,1,1,'',10),(39,5,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(40,5,'created_at','timestamp','Created At',0,1,1,0,0,0,'',12),(41,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',13),(42,5,'seo_title','text','SEO Title',0,1,1,1,1,1,'',14),(43,5,'featured','checkbox','Featured',1,1,1,1,1,1,'',15),(44,6,'id','number','ID',1,0,0,0,0,0,'',1),(45,6,'author_id','text','Author',1,0,0,0,0,0,'',2),(46,6,'title','text','Title',1,1,1,1,1,1,'',3),(47,6,'excerpt','text_area','Excerpt',1,0,1,1,1,1,'',4),(48,6,'body','rich_text_box','Body',1,0,1,1,1,1,'',5),(49,6,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}',6),(50,6,'meta_description','text','Meta Description',1,0,1,1,1,1,'',7),(51,6,'meta_keywords','text','Meta Keywords',1,0,1,1,1,1,'',8),(52,6,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(53,6,'created_at','timestamp','Created At',1,1,1,0,0,0,'',10),(54,6,'updated_at','timestamp','Updated At',1,0,0,0,0,0,'',11),(55,6,'image','image','Page Image',0,1,1,1,1,1,'',12),(56,7,'id','text','Id',1,0,0,0,0,0,NULL,1),(57,7,'mobile','text','Mobile',0,1,1,1,1,1,NULL,2),(58,7,'email','text','Email',0,1,1,1,1,1,NULL,3),(59,7,'address','text','Address',0,1,1,1,1,1,NULL,4),(60,7,'map','coordinates','Map',0,1,1,1,1,1,NULL,5),(61,7,'facebook','text','Facebook',0,1,1,1,1,1,NULL,6),(62,7,'twitter','text','Twitter',0,1,1,1,1,1,NULL,7),(63,7,'google','text','Google',0,1,1,1,1,1,NULL,8),(64,8,'id','text','Id',1,0,0,0,0,0,NULL,1),(65,8,'text','text_area','Text',0,1,1,1,1,1,NULL,3),(66,8,'picture','image','Picture',0,1,1,1,1,1,NULL,4),(67,8,'title','text','Title',0,1,1,1,1,1,NULL,2),(68,9,'id','text','Id',1,0,0,0,0,0,NULL,1),(69,9,'picture','image','Picture',0,1,1,1,1,1,NULL,2),(70,9,'title','text','Title',0,1,1,1,1,1,NULL,3),(71,9,'description','text_area','Description',0,1,1,1,1,1,NULL,4),(72,9,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,5),(73,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(74,10,'id','text','Id',1,0,0,0,0,0,NULL,1),(75,10,'icon','text','Icon',0,1,1,1,1,1,NULL,2),(76,10,'title','text','Title',0,1,1,1,1,1,NULL,3),(77,10,'description','text_area','Description',0,1,1,1,1,1,NULL,4),(78,10,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,5),(79,10,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(80,11,'id','text','Id',1,0,0,0,0,0,NULL,1),(81,11,'title1','text','Title1',0,1,1,1,1,1,NULL,2),(82,11,'title2','text','Title2',0,1,1,1,1,1,NULL,3),(83,11,'picture','image','Picture',0,1,1,1,1,1,NULL,4),(84,11,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,5),(85,11,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(86,12,'id','text','Id',1,0,0,0,0,0,NULL,1),(87,12,'picture','image','Picture',0,1,1,1,1,1,NULL,2),(88,12,'title','text','Title',0,1,1,1,1,1,NULL,3),(89,12,'body','rich_text_box','Body',0,1,1,1,1,1,NULL,4),(90,12,'post_date','date','Post Date',0,1,1,1,1,1,NULL,5),(91,12,'author_name','text','Author Name',0,1,1,1,1,1,NULL,6),(92,12,'author_picture','image','Author Picture',0,1,1,1,1,1,NULL,7),(93,12,'author_description','text','Author Description',0,1,1,1,1,1,NULL,8),(94,12,'author_facebook','text','Author Facebook',0,1,1,1,1,1,NULL,9),(95,12,'author_twitter','text','Author Twitter',0,1,1,1,1,1,NULL,10),(96,12,'author_googleplus','text','Author Googleplus',0,1,1,1,1,1,NULL,11),(97,12,'video','text','Video',0,1,1,1,1,1,NULL,12),(98,12,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,14),(99,12,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,15),(100,13,'id','text','Id',1,0,0,0,0,0,NULL,1),(101,13,'name','text','Name',0,1,1,1,1,1,NULL,2),(102,13,'email','text','Email',0,1,1,1,1,1,NULL,3),(103,13,'subject','text','Subject',0,1,1,1,1,1,NULL,4),(104,13,'message','text_area','Message',0,1,1,1,1,1,NULL,5),(105,13,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(106,13,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(107,14,'id','text','Id',1,0,0,0,0,0,NULL,1),(108,14,'name','text','Name',0,1,1,1,1,1,NULL,2),(109,14,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,4),(110,14,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,5),(111,14,'slug','text','Slug',0,1,1,1,1,0,'{\"slugify\":{\"origin\":\"name\"}}',3),(112,15,'id','text','Id',1,0,0,0,0,0,NULL,1),(113,15,'title','text','Title',0,1,1,1,1,1,NULL,2),(114,15,'picture','image','Picture',0,1,1,1,1,1,NULL,3),(115,15,'text','rich_text_box','Text',0,1,1,1,1,1,NULL,4),(116,15,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(117,15,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(118,15,'category_id','select_dropdown','Category Id',0,1,1,1,1,1,NULL,5),(119,15,'gallery_image_belongsto_gallery_category_relationship','relationship','gallery_categories',0,1,1,1,1,1,'{\"model\":\"App\\\\GalleryCategory\",\"table\":\"gallery_categories\",\"type\":\"belongsTo\",\"column\":\"category_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"about\",\"pivot\":\"0\",\"taggable\":\"0\"}',8),(120,15,'facebook','text','Facebook',0,1,1,1,1,1,NULL,8),(121,15,'twitter','text','Twitter',0,1,1,1,1,1,NULL,9),(122,15,'googleplus','text','Googleplus',0,1,1,1,1,1,NULL,10),(123,15,'linkedin','text','Linkedin',0,1,1,1,1,1,NULL,11),(124,15,'pinterest','text','Pinterest',0,1,1,1,1,1,NULL,12),(125,7,'youtube','text','Youtube',0,1,1,1,1,1,NULL,9),(126,16,'id','text','Id',1,0,0,0,0,0,NULL,1),(127,16,'picture','image','Picture',0,1,1,1,1,1,NULL,2),(128,16,'title1','text','Title1',0,1,1,1,1,1,NULL,3),(129,16,'title2','text','Title2',0,1,1,1,1,1,NULL,4),(130,16,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,5),(131,16,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(132,17,'id','text','Id',1,0,0,0,0,0,NULL,1),(133,17,'picture','image','Picture',0,1,1,1,1,1,NULL,2),(134,17,'title','text','Title',0,1,1,1,1,1,NULL,3),(135,17,'body','text_area','Body',0,1,1,1,1,1,NULL,4),(136,17,'button_text','text','Button Text',0,1,1,1,1,1,NULL,5),(137,17,'button_link','text','Button Link',0,1,1,1,1,1,NULL,6),(138,17,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,7),(139,17,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,8),(140,12,'user_id','text','User Id',0,0,0,0,0,0,NULL,13),(141,18,'id','text','Id',1,0,0,0,0,0,NULL,1),(142,18,'picture','image','Picture',0,1,1,1,1,1,NULL,2),(143,18,'title','text','Title',0,1,1,1,1,1,NULL,3),(144,18,'user','text','User',0,1,1,1,1,1,NULL,4),(145,18,'text','text','Text',0,1,1,1,1,1,NULL,5),(146,18,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(147,18,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(148,19,'id','text','Id',1,0,0,0,0,0,NULL,1),(149,19,'title','text','Title',0,1,1,1,1,1,NULL,2),(150,19,'start_date','date','Start Date',0,1,1,1,1,1,NULL,3),(151,19,'end_date','date','End Date',0,1,1,1,1,1,NULL,4),(152,19,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,5),(153,19,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(154,20,'id','text','Id',1,0,0,0,0,0,NULL,1),(155,20,'title','text','Title',0,1,1,1,1,1,NULL,2),(156,20,'description','text_area','Description',0,1,1,1,1,1,NULL,3),(157,20,'images','multiple_images','Images',0,1,1,1,1,1,NULL,4),(158,20,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,5),(159,20,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(160,21,'id','text','Id',1,0,0,0,0,0,NULL,1),(161,21,'title','text','Title',0,1,1,1,1,1,NULL,2),(162,21,'name','text','Name',0,1,1,1,1,1,NULL,3),(163,21,'text1','text','Text1',0,1,1,1,1,1,NULL,4),(164,21,'text2','text','Text2',0,1,1,1,1,1,NULL,5),(165,21,'text3','text','Text3',0,1,1,1,1,1,NULL,6),(166,21,'text4','text','Text4',0,1,1,1,1,1,NULL,7),(167,21,'text5','text','Text5',0,1,1,1,1,1,NULL,8),(168,21,'button_text','text','Button Text',0,1,1,1,1,1,NULL,9),(169,21,'button_link','text','Button Link',0,1,1,1,1,1,NULL,10),(170,21,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,11),(171,21,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,12);

/*Table structure for table `data_types` */

DROP TABLE IF EXISTS `data_types`;

CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `data_types` */

insert  into `data_types`(`id`,`name`,`slug`,`display_name_singular`,`display_name_plural`,`icon`,`model_name`,`policy_name`,`controller`,`description`,`generate_permissions`,`server_side`,`details`,`created_at`,`updated_at`) values (1,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','','',1,0,NULL,'2018-04-28 11:03:26','2018-04-28 11:03:26'),(2,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,NULL,'2018-04-28 11:03:26','2018-04-28 11:03:26'),(3,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,NULL,'2018-04-28 11:03:27','2018-04-28 11:03:27'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,NULL,'2018-04-28 11:03:41','2018-04-28 11:03:41'),(5,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy','','',1,0,NULL,'2018-04-28 11:03:43','2018-04-28 11:03:43'),(6,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,NULL,'2018-04-28 11:03:45','2018-04-28 11:03:45'),(7,'information','information','Information','Information',NULL,'App\\Information',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-28 14:52:48','2018-04-28 14:52:48'),(8,'about','about','About','Abouts',NULL,'App\\About',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-28 15:54:56','2018-04-28 15:54:56'),(9,'quotes','quotes','Quote','Quotes',NULL,'App\\Quote',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-30 19:48:57','2018-04-30 19:48:57'),(10,'about_points','about-points','About Point','About Points',NULL,'App\\AboutPoint',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-04-30 20:04:19','2018-04-30 20:04:19'),(11,'blog_slides','blog-slides','Blog Slide','Blog Slides',NULL,'App\\BlogSlide',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-02 17:19:32','2018-05-02 17:19:32'),(12,'articles','articles','Article','Articles',NULL,'App\\Article',NULL,'CustomArticlesController',NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-02 18:42:19','2018-05-07 20:38:27'),(13,'messages','messages','Message','Messages',NULL,'App\\Message',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-04 15:05:47','2018-05-04 15:05:47'),(14,'gallery_categories','gallery-categories','Gallery Category','Gallery Categories',NULL,'App\\GalleryCategory',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-04 15:14:29','2018-05-04 15:14:29'),(15,'gallery_images','gallery-images','Gallery Image','Gallery Images',NULL,'App\\GalleryImage',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-04 15:34:21','2018-05-04 15:34:21'),(16,'slides','slides','Slide','Slides',NULL,'App\\Slide',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-04 17:29:21','2018-05-04 17:29:21'),(17,'main_blocks','main-blocks','Main Block','Main Blocks',NULL,'App\\MainBlock',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-04 17:37:00','2018-05-04 17:37:00'),(18,'courses','courses','Course','Courses',NULL,'App\\Course',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-08 19:55:12','2018-05-08 19:55:12'),(19,'events','events','Event','Events',NULL,'App\\Event',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-08 20:19:47','2018-05-08 20:19:47'),(20,'stories','stories','Story','Stories',NULL,'App\\Story',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-08 21:21:18','2018-05-08 21:21:18'),(21,'packages','packages','Package','Packages',NULL,'App\\Package',NULL,NULL,NULL,1,0,'{\"order_column\":null,\"order_display_column\":null}','2018-05-08 22:30:25','2018-05-08 22:30:25');

/*Table structure for table `events` */

DROP TABLE IF EXISTS `events`;

CREATE TABLE `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `events` */

insert  into `events`(`id`,`title`,`start_date`,`end_date`,`created_at`,`updated_at`) values (1,'Demo Event-1','2018-05-10','2018-05-11','2018-05-08 20:24:53','2018-05-08 20:24:53'),(2,'Demo Event-2','2017-09-11','2017-09-13','2018-05-08 20:24:53','2018-05-08 20:24:53'),(3,'Demo Event-3','2017-09-14','2017-09-14','2018-05-08 20:24:54','2018-05-08 20:24:54'),(4,'Demo Event-3','2017-09-17','2017-09-17','2018-05-08 20:24:54','2018-05-08 20:24:54');

/*Table structure for table `gallery_categories` */

DROP TABLE IF EXISTS `gallery_categories`;

CREATE TABLE `gallery_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `gallery_categories` */

insert  into `gallery_categories`(`id`,`name`,`created_at`,`updated_at`,`slug`) values (1,'test1','2018-05-04 15:16:05','2018-05-04 15:28:23','test1'),(2,'test2','2018-05-04 15:16:30','2018-05-04 15:28:15','test2');

/*Table structure for table `gallery_images` */

DROP TABLE IF EXISTS `gallery_images`;

CREATE TABLE `gallery_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `googleplus` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pinterest` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `gallery_images` */

insert  into `gallery_images`(`id`,`title`,`picture`,`text`,`created_at`,`updated_at`,`category_id`,`facebook`,`twitter`,`googleplus`,`linkedin`,`pinterest`) values (1,'IMAGE TITLE','gallery-images\\May2018\\VymFVSVtK0aOodHmpmtz.jpg','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iste placeat, deleniti. Veritatis, praesentium, et! Voluptate iusto aliquid quis, optio dolore quas non eveniet aut. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque debitis fuga eum, velit deleniti perferendis fugit necessitatibus inventore expedita accusantium animi provident aspernatur, accusamus in nobis illo? Exercitationem, deserunt esse. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet minima laboriosam dolores sunt pariatur quia, non maxime excepturi expedita, animi rem earum temporibus quidem molestias. Aut blanditiis, nulla esse accusamus. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet minima laboriosam dolores sunt pariatur quia, non maxime excepturi expedita, animi rem earum temporibus quidem molestias. Aut blanditiis, nulla esse accusamus.</p>','2018-05-04 15:36:18','2018-05-04 15:40:24',1,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `information` */

DROP TABLE IF EXISTS `information`;

CREATE TABLE `information` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `map` geometry DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `youtube` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `information` */

insert  into `information`(`id`,`mobile`,`email`,`address`,`map`,`facebook`,`twitter`,`google`,`youtube`) values (1,'8 (800) 695-2686','support@lifeguide.com','Baker st. 567, San Diego, CA','\0\0\0\0\0\0\0U�23;?@���Ͽ>@',NULL,NULL,NULL,NULL);

/*Table structure for table `main_blocks` */

DROP TABLE IF EXISTS `main_blocks`;

CREATE TABLE `main_blocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `button_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `button_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `main_blocks` */

insert  into `main_blocks`(`id`,`picture`,`title`,`body`,`button_text`,`button_link`,`created_at`,`updated_at`) values (1,'main-blocks\\May2018\\X20oMHJ85WLYU3CUJO2C.jpg','title','body body body ','see more','#','2018-05-04 17:38:56','2018-05-04 17:38:56');

/*Table structure for table `menu_items` */

DROP TABLE IF EXISTS `menu_items`;

CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `menu_items` */

insert  into `menu_items`(`id`,`menu_id`,`title`,`url`,`target`,`icon_class`,`color`,`parent_id`,`order`,`created_at`,`updated_at`,`route`,`parameters`) values (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2018-04-28 11:03:30','2018-04-28 11:03:30','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,4,'2018-04-28 11:03:30','2018-05-11 21:23:10','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,NULL,3,'2018-04-28 11:03:30','2018-04-28 11:03:30','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2018-04-28 11:03:30','2018-04-28 11:03:30','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,8,'2018-04-28 11:03:30','2018-05-11 21:23:10',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,1,'2018-04-28 11:03:30','2018-05-11 21:23:10','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,2,'2018-04-28 11:03:30','2018-05-11 21:23:11','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,3,'2018-04-28 11:03:30','2018-05-11 21:23:11','voyager.compass.index',NULL),(9,1,'Settings','','_self','voyager-settings',NULL,NULL,9,'2018-04-28 11:03:31','2018-05-11 21:23:11','voyager.settings.index',NULL),(10,1,'Categories','','_self','voyager-categories',NULL,NULL,7,'2018-04-28 11:03:42','2018-05-11 21:23:10','voyager.categories.index',NULL),(11,1,'Posts','','_self','voyager-news',NULL,NULL,5,'2018-04-28 11:03:44','2018-05-11 21:23:10','voyager.posts.index',NULL),(12,1,'Pages','','_self','voyager-file-text',NULL,NULL,6,'2018-04-28 11:03:46','2018-05-11 21:23:10','voyager.pages.index',NULL),(13,1,'Hooks','','_self','voyager-hook',NULL,5,4,'2018-04-28 11:03:53','2018-05-11 21:23:11','voyager.hooks',NULL),(14,1,'Main Information','','_self','voyager-exclamation','#000000',NULL,15,'2018-04-28 14:52:48','2018-05-11 21:41:49','voyager.information.index','null'),(15,1,'Main paragraph','','_self','voyager-file-text','#000000',29,1,'2018-04-28 15:54:56','2018-05-11 21:25:53','voyager.about.index','null'),(16,1,'Quotes','','_self','voyager-documentation','#000000',29,2,'2018-04-30 19:48:57','2018-05-11 21:28:00','voyager.quotes.index','null'),(17,1,'Points','','_self','voyager-dot-3','#000000',29,3,'2018-04-30 20:04:20','2018-05-11 21:26:39','voyager.about-points.index','null'),(18,1,'Slide Show','','_self','voyager-images','#000000',30,1,'2018-05-02 17:19:32','2018-05-11 21:29:30','voyager.blog-slides.index','null'),(19,1,'Articles','','_self','voyager-documentation','#000000',30,2,'2018-05-02 18:42:20','2018-05-11 21:29:52','voyager.articles.index','null'),(20,1,'Messages','','_self','voyager-edit','#000000',NULL,14,'2018-05-04 15:05:47','2018-05-11 21:41:49','voyager.messages.index','null'),(21,1,'Gallery Gategories','','_self','voyager-categories','#000000',31,1,'2018-05-04 15:14:29','2018-05-11 21:32:43','voyager.gallery-categories.index','null'),(22,1,'Images','','_self','voyager-images','#000000',31,2,'2018-05-04 15:34:21','2018-05-11 21:33:06','voyager.gallery-images.index','null'),(23,1,'Slide Show','','_self','voyager-images','#000000',32,1,'2018-05-04 17:29:21','2018-05-11 21:34:57','voyager.slides.index','null'),(24,1,'Main Blocks','','_self','voyager-dot-3','#000000',32,2,'2018-05-04 17:37:01','2018-05-11 21:35:43','voyager.main-blocks.index','null'),(25,1,'Courses','','_self','voyager-treasure','#000000',32,3,'2018-05-08 19:55:13','2018-05-11 21:36:05','voyager.courses.index','null'),(26,1,'Events','','_self','voyager-lifebuoy','#000000',32,4,'2018-05-08 20:19:48','2018-05-11 21:36:41','voyager.events.index','null'),(27,1,'Stories','','_self','voyager-volume-up','#000000',32,5,'2018-05-08 21:21:18','2018-05-11 21:37:06','voyager.stories.index','null'),(28,1,'Packages','','_self','voyager-tree','#000000',32,6,'2018-05-08 22:30:25','2018-05-11 21:37:27','voyager.packages.index','null'),(29,1,'About Page','','_self','voyager-question','#000000',NULL,11,'2018-05-11 21:22:56','2018-05-11 21:38:20',NULL,''),(30,1,'Blog','','_self','voyager-documentation','#000000',NULL,12,'2018-05-11 21:28:32','2018-05-11 21:41:15',NULL,''),(31,1,'Gallery','','_self','voyager-images','#000000',NULL,13,'2018-05-11 21:32:04','2018-05-11 21:41:30',NULL,''),(32,1,'Home page','','_self','voyager-home','#000000',NULL,10,'2018-05-11 21:33:37','2018-05-11 21:37:43',NULL,'');

/*Table structure for table `menus` */

DROP TABLE IF EXISTS `menus`;

CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `menus` */

insert  into `menus`(`id`,`name`,`created_at`,`updated_at`) values (1,'admin','2018-04-28 11:03:30','2018-04-28 11:03:30');

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `messages` */

insert  into `messages`(`id`,`name`,`email`,`subject`,`message`,`created_at`,`updated_at`) values (1,'فثسف','t@t.com','test','test','2018-05-04 15:10:08','2018-05-04 15:10:08');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_05_19_173453_create_menu_table',1),(6,'2016_10_21_190000_create_roles_table',1),(7,'2016_10_21_190000_create_settings_table',1),(8,'2016_11_30_135954_create_permission_table',1),(9,'2016_11_30_141208_create_permission_role_table',1),(10,'2016_12_26_201236_data_types__add__server_side',1),(11,'2017_01_13_000000_add_route_to_menu_items_table',1),(12,'2017_01_14_005015_create_translations_table',1),(13,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(14,'2017_03_06_000000_add_controller_to_data_types_table',1),(15,'2017_04_21_000000_add_order_to_data_rows_table',1),(16,'2017_07_05_210000_add_policyname_to_data_types_table',1),(17,'2017_08_05_000000_add_group_to_settings_table',1),(18,'2017_11_26_013050_add_user_role_relationship',1),(19,'2017_11_26_015000_create_user_roles_table',1),(20,'2018_03_11_000000_add_user_settings',1),(21,'2018_03_14_000000_add_details_to_data_types_table',1),(22,'2018_03_16_000000_make_settings_value_nullable',1),(23,'2016_01_01_000000_create_pages_table',2),(24,'2016_01_01_000000_create_posts_table',2),(25,'2016_02_15_204651_create_categories_table',2),(26,'2017_04_11_000000_alter_post_nullable_fields_table',2);

/*Table structure for table `packages` */

DROP TABLE IF EXISTS `packages`;

CREATE TABLE `packages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text4` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text5` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `button_text` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `button_link` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `packages` */

insert  into `packages`(`id`,`title`,`name`,`text1`,`text2`,`text3`,`text4`,`text5`,`button_text`,`button_link`,`created_at`,`updated_at`) values (1,'silver','99','test','test','test','test','test','test','#','2018-05-08 22:32:15','2018-05-08 22:32:15');

/*Table structure for table `pages` */

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `pages` */

insert  into `pages`(`id`,`author_id`,`title`,`excerpt`,`body`,`image`,`slug`,`meta_description`,`meta_keywords`,`status`,`created_at`,`updated_at`) values (1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/page1.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2018-04-28 11:03:47','2018-04-28 11:03:47');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`permission_id`,`role_id`) values (1,1),(1,2),(1,3),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(42,1),(42,3),(43,1),(43,3),(44,1),(44,3),(45,1),(46,1),(47,1),(47,3),(48,1),(48,3),(49,1),(49,3),(50,1),(51,1),(52,1),(52,3),(53,1),(53,3),(54,1),(54,3),(55,1),(55,3),(56,1),(56,3),(57,1),(57,3),(58,1),(58,3),(59,1),(59,3),(60,1),(60,3),(61,1),(61,3),(62,1),(62,3),(63,1),(63,3),(64,1),(64,3),(65,1),(65,3),(66,1),(66,3),(67,1),(67,2),(67,3),(68,1),(68,2),(68,3),(69,1),(69,2),(69,3),(70,1),(70,2),(70,3),(71,1),(71,2),(71,3),(72,1),(72,3),(73,1),(73,3),(74,1),(75,1),(76,1),(77,1),(77,3),(78,1),(78,3),(79,1),(79,3),(80,1),(80,3),(81,1),(81,3),(82,1),(82,3),(83,1),(83,3),(84,1),(84,3),(85,1),(85,3),(86,1),(86,3),(87,1),(87,3),(88,1),(88,3),(89,1),(89,3),(90,1),(90,3),(91,1),(91,3),(92,1),(92,3),(93,1),(93,3),(94,1),(94,3),(95,1),(95,3),(96,1),(96,3),(97,1),(97,3),(98,1),(98,3),(99,1),(99,3),(100,1),(100,3),(101,1),(101,3),(102,1),(102,3),(103,1),(103,3),(104,1),(104,3),(105,1),(105,3),(106,1),(106,3),(107,1),(107,3),(108,1),(108,3),(109,1),(109,3),(110,1),(110,3),(111,1),(111,3),(112,1),(112,3),(113,1),(113,3),(114,1),(114,3),(115,1),(115,3),(116,1),(116,3);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`key`,`table_name`,`created_at`,`updated_at`) values (1,'browse_admin',NULL,'2018-04-28 11:03:31','2018-04-28 11:03:31'),(2,'browse_bread',NULL,'2018-04-28 11:03:31','2018-04-28 11:03:31'),(3,'browse_database',NULL,'2018-04-28 11:03:31','2018-04-28 11:03:31'),(4,'browse_media',NULL,'2018-04-28 11:03:31','2018-04-28 11:03:31'),(5,'browse_compass',NULL,'2018-04-28 11:03:32','2018-04-28 11:03:32'),(6,'browse_menus','menus','2018-04-28 11:03:32','2018-04-28 11:03:32'),(7,'read_menus','menus','2018-04-28 11:03:32','2018-04-28 11:03:32'),(8,'edit_menus','menus','2018-04-28 11:03:32','2018-04-28 11:03:32'),(9,'add_menus','menus','2018-04-28 11:03:32','2018-04-28 11:03:32'),(10,'delete_menus','menus','2018-04-28 11:03:32','2018-04-28 11:03:32'),(11,'browse_roles','roles','2018-04-28 11:03:32','2018-04-28 11:03:32'),(12,'read_roles','roles','2018-04-28 11:03:32','2018-04-28 11:03:32'),(13,'edit_roles','roles','2018-04-28 11:03:32','2018-04-28 11:03:32'),(14,'add_roles','roles','2018-04-28 11:03:32','2018-04-28 11:03:32'),(15,'delete_roles','roles','2018-04-28 11:03:32','2018-04-28 11:03:32'),(16,'browse_users','users','2018-04-28 11:03:32','2018-04-28 11:03:32'),(17,'read_users','users','2018-04-28 11:03:33','2018-04-28 11:03:33'),(18,'edit_users','users','2018-04-28 11:03:33','2018-04-28 11:03:33'),(19,'add_users','users','2018-04-28 11:03:33','2018-04-28 11:03:33'),(20,'delete_users','users','2018-04-28 11:03:33','2018-04-28 11:03:33'),(21,'browse_settings','settings','2018-04-28 11:03:33','2018-04-28 11:03:33'),(22,'read_settings','settings','2018-04-28 11:03:33','2018-04-28 11:03:33'),(23,'edit_settings','settings','2018-04-28 11:03:33','2018-04-28 11:03:33'),(24,'add_settings','settings','2018-04-28 11:03:33','2018-04-28 11:03:33'),(25,'delete_settings','settings','2018-04-28 11:03:33','2018-04-28 11:03:33'),(26,'browse_categories','categories','2018-04-28 11:03:42','2018-04-28 11:03:42'),(27,'read_categories','categories','2018-04-28 11:03:42','2018-04-28 11:03:42'),(28,'edit_categories','categories','2018-04-28 11:03:42','2018-04-28 11:03:42'),(29,'add_categories','categories','2018-04-28 11:03:42','2018-04-28 11:03:42'),(30,'delete_categories','categories','2018-04-28 11:03:42','2018-04-28 11:03:42'),(31,'browse_posts','posts','2018-04-28 11:03:44','2018-04-28 11:03:44'),(32,'read_posts','posts','2018-04-28 11:03:44','2018-04-28 11:03:44'),(33,'edit_posts','posts','2018-04-28 11:03:44','2018-04-28 11:03:44'),(34,'add_posts','posts','2018-04-28 11:03:45','2018-04-28 11:03:45'),(35,'delete_posts','posts','2018-04-28 11:03:45','2018-04-28 11:03:45'),(36,'browse_pages','pages','2018-04-28 11:03:46','2018-04-28 11:03:46'),(37,'read_pages','pages','2018-04-28 11:03:47','2018-04-28 11:03:47'),(38,'edit_pages','pages','2018-04-28 11:03:47','2018-04-28 11:03:47'),(39,'add_pages','pages','2018-04-28 11:03:47','2018-04-28 11:03:47'),(40,'delete_pages','pages','2018-04-28 11:03:47','2018-04-28 11:03:47'),(41,'browse_hooks',NULL,'2018-04-28 11:03:53','2018-04-28 11:03:53'),(42,'browse_information','information','2018-04-28 14:52:48','2018-04-28 14:52:48'),(43,'read_information','information','2018-04-28 14:52:48','2018-04-28 14:52:48'),(44,'edit_information','information','2018-04-28 14:52:48','2018-04-28 14:52:48'),(45,'add_information','information','2018-04-28 14:52:48','2018-04-28 14:52:48'),(46,'delete_information','information','2018-04-28 14:52:48','2018-04-28 14:52:48'),(47,'browse_about','about','2018-04-28 15:54:56','2018-04-28 15:54:56'),(48,'read_about','about','2018-04-28 15:54:56','2018-04-28 15:54:56'),(49,'edit_about','about','2018-04-28 15:54:56','2018-04-28 15:54:56'),(50,'add_about','about','2018-04-28 15:54:56','2018-04-28 15:54:56'),(51,'delete_about','about','2018-04-28 15:54:56','2018-04-28 15:54:56'),(52,'browse_quotes','quotes','2018-04-30 19:48:57','2018-04-30 19:48:57'),(53,'read_quotes','quotes','2018-04-30 19:48:57','2018-04-30 19:48:57'),(54,'edit_quotes','quotes','2018-04-30 19:48:57','2018-04-30 19:48:57'),(55,'add_quotes','quotes','2018-04-30 19:48:57','2018-04-30 19:48:57'),(56,'delete_quotes','quotes','2018-04-30 19:48:57','2018-04-30 19:48:57'),(57,'browse_about_points','about_points','2018-04-30 20:04:20','2018-04-30 20:04:20'),(58,'read_about_points','about_points','2018-04-30 20:04:20','2018-04-30 20:04:20'),(59,'edit_about_points','about_points','2018-04-30 20:04:20','2018-04-30 20:04:20'),(60,'add_about_points','about_points','2018-04-30 20:04:20','2018-04-30 20:04:20'),(61,'delete_about_points','about_points','2018-04-30 20:04:20','2018-04-30 20:04:20'),(62,'browse_blog_slides','blog_slides','2018-05-02 17:19:32','2018-05-02 17:19:32'),(63,'read_blog_slides','blog_slides','2018-05-02 17:19:32','2018-05-02 17:19:32'),(64,'edit_blog_slides','blog_slides','2018-05-02 17:19:32','2018-05-02 17:19:32'),(65,'add_blog_slides','blog_slides','2018-05-02 17:19:32','2018-05-02 17:19:32'),(66,'delete_blog_slides','blog_slides','2018-05-02 17:19:32','2018-05-02 17:19:32'),(67,'browse_articles','articles','2018-05-02 18:42:20','2018-05-02 18:42:20'),(68,'read_articles','articles','2018-05-02 18:42:20','2018-05-02 18:42:20'),(69,'edit_articles','articles','2018-05-02 18:42:20','2018-05-02 18:42:20'),(70,'add_articles','articles','2018-05-02 18:42:20','2018-05-02 18:42:20'),(71,'delete_articles','articles','2018-05-02 18:42:20','2018-05-02 18:42:20'),(72,'browse_messages','messages','2018-05-04 15:05:47','2018-05-04 15:05:47'),(73,'read_messages','messages','2018-05-04 15:05:47','2018-05-04 15:05:47'),(74,'edit_messages','messages','2018-05-04 15:05:47','2018-05-04 15:05:47'),(75,'add_messages','messages','2018-05-04 15:05:47','2018-05-04 15:05:47'),(76,'delete_messages','messages','2018-05-04 15:05:47','2018-05-04 15:05:47'),(77,'browse_gallery_categories','gallery_categories','2018-05-04 15:14:29','2018-05-04 15:14:29'),(78,'read_gallery_categories','gallery_categories','2018-05-04 15:14:29','2018-05-04 15:14:29'),(79,'edit_gallery_categories','gallery_categories','2018-05-04 15:14:29','2018-05-04 15:14:29'),(80,'add_gallery_categories','gallery_categories','2018-05-04 15:14:29','2018-05-04 15:14:29'),(81,'delete_gallery_categories','gallery_categories','2018-05-04 15:14:29','2018-05-04 15:14:29'),(82,'browse_gallery_images','gallery_images','2018-05-04 15:34:21','2018-05-04 15:34:21'),(83,'read_gallery_images','gallery_images','2018-05-04 15:34:21','2018-05-04 15:34:21'),(84,'edit_gallery_images','gallery_images','2018-05-04 15:34:21','2018-05-04 15:34:21'),(85,'add_gallery_images','gallery_images','2018-05-04 15:34:21','2018-05-04 15:34:21'),(86,'delete_gallery_images','gallery_images','2018-05-04 15:34:21','2018-05-04 15:34:21'),(87,'browse_slides','slides','2018-05-04 17:29:21','2018-05-04 17:29:21'),(88,'read_slides','slides','2018-05-04 17:29:21','2018-05-04 17:29:21'),(89,'edit_slides','slides','2018-05-04 17:29:21','2018-05-04 17:29:21'),(90,'add_slides','slides','2018-05-04 17:29:21','2018-05-04 17:29:21'),(91,'delete_slides','slides','2018-05-04 17:29:21','2018-05-04 17:29:21'),(92,'browse_main_blocks','main_blocks','2018-05-04 17:37:01','2018-05-04 17:37:01'),(93,'read_main_blocks','main_blocks','2018-05-04 17:37:01','2018-05-04 17:37:01'),(94,'edit_main_blocks','main_blocks','2018-05-04 17:37:01','2018-05-04 17:37:01'),(95,'add_main_blocks','main_blocks','2018-05-04 17:37:01','2018-05-04 17:37:01'),(96,'delete_main_blocks','main_blocks','2018-05-04 17:37:01','2018-05-04 17:37:01'),(97,'browse_courses','courses','2018-05-08 19:55:13','2018-05-08 19:55:13'),(98,'read_courses','courses','2018-05-08 19:55:13','2018-05-08 19:55:13'),(99,'edit_courses','courses','2018-05-08 19:55:13','2018-05-08 19:55:13'),(100,'add_courses','courses','2018-05-08 19:55:13','2018-05-08 19:55:13'),(101,'delete_courses','courses','2018-05-08 19:55:13','2018-05-08 19:55:13'),(102,'browse_events','events','2018-05-08 20:19:48','2018-05-08 20:19:48'),(103,'read_events','events','2018-05-08 20:19:48','2018-05-08 20:19:48'),(104,'edit_events','events','2018-05-08 20:19:48','2018-05-08 20:19:48'),(105,'add_events','events','2018-05-08 20:19:48','2018-05-08 20:19:48'),(106,'delete_events','events','2018-05-08 20:19:48','2018-05-08 20:19:48'),(107,'browse_stories','stories','2018-05-08 21:21:18','2018-05-08 21:21:18'),(108,'read_stories','stories','2018-05-08 21:21:18','2018-05-08 21:21:18'),(109,'edit_stories','stories','2018-05-08 21:21:18','2018-05-08 21:21:18'),(110,'add_stories','stories','2018-05-08 21:21:18','2018-05-08 21:21:18'),(111,'delete_stories','stories','2018-05-08 21:21:18','2018-05-08 21:21:18'),(112,'browse_packages','packages','2018-05-08 22:30:25','2018-05-08 22:30:25'),(113,'read_packages','packages','2018-05-08 22:30:25','2018-05-08 22:30:25'),(114,'edit_packages','packages','2018-05-08 22:30:25','2018-05-08 22:30:25'),(115,'add_packages','packages','2018-05-08 22:30:25','2018-05-08 22:30:25'),(116,'delete_packages','packages','2018-05-08 22:30:25','2018-05-08 22:30:25');

/*Table structure for table `posts` */

DROP TABLE IF EXISTS `posts`;

CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `posts` */

insert  into `posts`(`id`,`author_id`,`category_id`,`title`,`seo_title`,`excerpt`,`body`,`image`,`slug`,`meta_description`,`meta_keywords`,`status`,`featured`,`created_at`,`updated_at`) values (1,0,NULL,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/post1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2018-04-28 11:03:45','2018-04-28 11:03:45'),(2,0,NULL,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>','posts/post2.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2018-04-28 11:03:45','2018-04-28 11:03:45'),(3,0,NULL,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/post3.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2018-04-28 11:03:45','2018-04-28 11:03:45'),(4,0,NULL,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/post4.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2018-04-28 11:03:45','2018-04-28 11:03:45');

/*Table structure for table `quotes` */

DROP TABLE IF EXISTS `quotes`;

CREATE TABLE `quotes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `quotes` */

insert  into `quotes`(`id`,`picture`,`title`,`description`,`created_at`,`updated_at`) values (1,'quotes\\April2018\\mzTNxgGv6uw5OO4qaogM.jpg','أحمد جلال','وصف وصف','2018-04-30 19:56:32','2018-04-30 19:56:32');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`display_name`,`created_at`,`updated_at`) values (1,'admin','Administrator','2018-04-28 11:03:31','2018-04-28 11:03:31'),(2,'user','Normal User','2018-04-28 11:03:31','2018-04-28 11:03:31'),(3,'Moderator','Moderator','2018-05-11 21:43:59','2018-05-11 21:43:59');

/*Table structure for table `settings` */

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `settings` */

insert  into `settings`(`id`,`key`,`display_name`,`value`,`details`,`type`,`order`,`group`) values (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo',NULL,'','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID',NULL,'','text',4,'Site'),(5,'admin.bg_image','Admin Background Image',NULL,'','image',5,'Admin'),(6,'admin.title','Admin Title','Wold Tree','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to the admin panel for Wold Tree','','text',2,'Admin'),(8,'admin.loader','Admin Loader',NULL,'','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image',NULL,'','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)',NULL,'','text',1,'Admin');

/*Table structure for table `slides` */

DROP TABLE IF EXISTS `slides`;

CREATE TABLE `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `slides` */

insert  into `slides`(`id`,`picture`,`title1`,`title2`,`created_at`,`updated_at`) values (1,'slides\\May2018\\PTrGA1tHVetv8mo8eVjh.jpg','discover','LIVING A LIFE WITH PASSION','2018-05-04 17:30:55','2018-05-04 17:30:55');

/*Table structure for table `stories` */

DROP TABLE IF EXISTS `stories`;

CREATE TABLE `stories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `images` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `stories` */

insert  into `stories`(`id`,`title`,`description`,`images`,`created_at`,`updated_at`) values (1,'Mary W. Johnson’s story:','At vero eos et accusam et justo duo dolores et ea rebum lorem ipsum dolor sit amet consetetur sadipscing elitr, seed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erated diam voluptua. At vero eos et accusam justo duo dolores et ea rebum.','[\"stories\\\\May2018\\\\SgBzAStbLNRhSRcK2Nnv.jpg\",\"stories\\\\May2018\\\\1LRDeIFvHMkFN7muT4hX.jpg\"]','2018-05-08 21:22:49','2018-05-08 21:22:49');

/*Table structure for table `translations` */

DROP TABLE IF EXISTS `translations`;

CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `translations` */

insert  into `translations`(`id`,`table_name`,`column_name`,`foreign_key`,`locale`,`value`,`created_at`,`updated_at`) values (1,'data_types','display_name_singular',5,'pt','Post','2018-04-28 11:03:47','2018-04-28 11:03:47'),(2,'data_types','display_name_singular',6,'pt','Página','2018-04-28 11:03:47','2018-04-28 11:03:47'),(3,'data_types','display_name_singular',1,'pt','Utilizador','2018-04-28 11:03:48','2018-04-28 11:03:48'),(4,'data_types','display_name_singular',4,'pt','Categoria','2018-04-28 11:03:48','2018-04-28 11:03:48'),(5,'data_types','display_name_singular',2,'pt','Menu','2018-04-28 11:03:48','2018-04-28 11:03:48'),(6,'data_types','display_name_singular',3,'pt','Função','2018-04-28 11:03:48','2018-04-28 11:03:48'),(7,'data_types','display_name_plural',5,'pt','Posts','2018-04-28 11:03:48','2018-04-28 11:03:48'),(8,'data_types','display_name_plural',6,'pt','Páginas','2018-04-28 11:03:48','2018-04-28 11:03:48'),(9,'data_types','display_name_plural',1,'pt','Utilizadores','2018-04-28 11:03:48','2018-04-28 11:03:48'),(10,'data_types','display_name_plural',4,'pt','Categorias','2018-04-28 11:03:48','2018-04-28 11:03:48'),(11,'data_types','display_name_plural',2,'pt','Menus','2018-04-28 11:03:48','2018-04-28 11:03:48'),(12,'data_types','display_name_plural',3,'pt','Funções','2018-04-28 11:03:48','2018-04-28 11:03:48'),(13,'categories','slug',1,'pt','categoria-1','2018-04-28 11:03:49','2018-04-28 11:03:49'),(14,'categories','name',1,'pt','Categoria 1','2018-04-28 11:03:49','2018-04-28 11:03:49'),(15,'categories','slug',2,'pt','categoria-2','2018-04-28 11:03:49','2018-04-28 11:03:49'),(16,'categories','name',2,'pt','Categoria 2','2018-04-28 11:03:49','2018-04-28 11:03:49'),(17,'pages','title',1,'pt','Olá Mundo','2018-04-28 11:03:49','2018-04-28 11:03:49'),(18,'pages','slug',1,'pt','ola-mundo','2018-04-28 11:03:49','2018-04-28 11:03:49'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2018-04-28 11:03:49','2018-04-28 11:03:49'),(20,'menu_items','title',1,'pt','Painel de Controle','2018-04-28 11:03:50','2018-04-28 11:03:50'),(21,'menu_items','title',2,'pt','Media','2018-04-28 11:03:50','2018-04-28 11:03:50'),(22,'menu_items','title',11,'pt','Publicações','2018-04-28 11:03:50','2018-04-28 11:03:50'),(23,'menu_items','title',3,'pt','Utilizadores','2018-04-28 11:03:50','2018-04-28 11:03:50'),(24,'menu_items','title',10,'pt','Categorias','2018-04-28 11:03:50','2018-04-28 11:03:50'),(25,'menu_items','title',12,'pt','Páginas','2018-04-28 11:03:50','2018-04-28 11:03:50'),(26,'menu_items','title',4,'pt','Funções','2018-04-28 11:03:50','2018-04-28 11:03:50'),(27,'menu_items','title',5,'pt','Ferramentas','2018-04-28 11:03:50','2018-04-28 11:03:50'),(28,'menu_items','title',6,'pt','Menus','2018-04-28 11:03:50','2018-04-28 11:03:50'),(29,'menu_items','title',7,'pt','Base de dados','2018-04-28 11:03:51','2018-04-28 11:03:51'),(30,'menu_items','title',9,'pt','Configurações','2018-04-28 11:03:51','2018-04-28 11:03:51'),(31,'information','address',1,'ar','test','2018-04-28 15:01:16','2018-04-28 15:01:16'),(32,'data_types','display_name_singular',7,'en','Information','2018-04-28 15:24:15','2018-04-28 15:24:15'),(33,'data_types','display_name_plural',7,'en','Information','2018-04-28 15:24:15','2018-04-28 15:24:15'),(34,'data_types','display_name_singular',8,'en','About','2018-04-28 15:55:11','2018-04-28 15:55:11'),(35,'data_types','display_name_plural',8,'en','Abouts','2018-04-28 15:55:11','2018-04-28 15:55:11'),(36,'about','text',1,'en','Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dgolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet lorem ipsum dolor sit amet.','2018-04-28 15:58:52','2018-04-28 15:58:52'),(37,'about','title',1,'en','World tree','2018-04-30 19:42:56','2018-04-30 19:42:56'),(38,'quotes','title',1,'en','Ahmed Galal','2018-04-30 19:56:32','2018-04-30 19:56:32'),(39,'quotes','description',1,'en','description','2018-04-30 19:56:32','2018-04-30 19:56:32'),(40,'about_points','title',1,'en','Being Happy','2018-04-30 20:06:43','2018-04-30 20:06:43'),(41,'about_points','description',1,'en','At vero eos et accusam et justo duo dolores','2018-04-30 20:06:43','2018-04-30 20:06:43'),(42,'about_points','title',2,'en','FULFILLING  POTENTIAL','2018-04-30 20:08:04','2018-04-30 20:08:04'),(43,'about_points','description',2,'en','test','2018-04-30 20:08:04','2018-04-30 20:08:04'),(44,'about_points','title',3,'en','test','2018-04-30 20:09:50','2018-04-30 20:09:50'),(45,'about_points','description',3,'en','test','2018-04-30 20:09:50','2018-04-30 20:09:50'),(46,'about_points','title',4,'en','test','2018-04-30 20:11:03','2018-04-30 20:11:03'),(47,'about_points','description',4,'en','test','2018-04-30 20:11:03','2018-04-30 20:11:03'),(48,'blog_slides','title1',1,'ar','اكتشف','2018-05-02 17:30:01','2018-05-02 17:30:01'),(49,'blog_slides','title2',1,'ar','سر الحياة','2018-05-02 17:30:01','2018-05-02 17:30:01'),(52,'articles','title',1,'ar','','2018-05-02 18:45:41','2018-05-02 18:45:41'),(53,'articles','body',1,'ar','','2018-05-02 18:45:41','2018-05-02 18:45:41'),(54,'articles','author_name',1,'ar','','2018-05-02 18:45:41','2018-05-02 18:45:41'),(55,'articles','author_description',1,'ar','','2018-05-02 18:45:41','2018-05-02 18:45:41'),(56,'gallery_categories','name',1,'ar','تيستت1','2018-05-04 15:16:05','2018-05-04 15:16:05'),(57,'gallery_categories','name',2,'ar','تيست2','2018-05-04 15:16:30','2018-05-04 15:16:30'),(58,'data_types','display_name_singular',14,'ar','Gallery Category','2018-05-04 15:26:28','2018-05-04 15:26:28'),(59,'data_types','display_name_plural',14,'ar','Gallery Categories','2018-05-04 15:26:28','2018-05-04 15:26:28'),(60,'gallery_images','title',1,'ar','','2018-05-04 15:36:18','2018-05-04 15:36:18'),(61,'gallery_images','text',1,'ar','','2018-05-04 15:36:18','2018-05-04 15:36:18'),(62,'data_types','display_name_singular',15,'ar','Gallery Image','2018-05-04 15:39:13','2018-05-04 15:39:13'),(63,'data_types','display_name_plural',15,'ar','Gallery Images','2018-05-04 15:39:13','2018-05-04 15:39:13'),(64,'data_types','display_name_singular',7,'ar','Information','2018-05-04 16:29:56','2018-05-04 16:29:56'),(65,'data_types','display_name_plural',7,'ar','Information','2018-05-04 16:29:56','2018-05-04 16:29:56'),(66,'slides','title1',1,'ar','اكتشف','2018-05-04 17:30:55','2018-05-04 17:30:55'),(67,'slides','title2',1,'ar','الحياة بشغف','2018-05-04 17:30:55','2018-05-04 17:30:55'),(68,'main_blocks','title',1,'ar','عنوان','2018-05-04 17:38:56','2018-05-04 17:38:56'),(69,'main_blocks','body',1,'ar','وصف وصف وصف','2018-05-04 17:38:56','2018-05-04 17:38:56'),(70,'main_blocks','button_text',1,'ar','المزيد','2018-05-04 17:38:56','2018-05-04 17:38:56'),(71,'data_types','display_name_singular',12,'ar','Article','2018-05-06 21:29:16','2018-05-06 21:29:16'),(72,'data_types','display_name_plural',12,'ar','Articles','2018-05-06 21:29:16','2018-05-06 21:29:16'),(73,'articles','title',6,'ar','','2018-05-06 22:02:42','2018-05-06 22:02:42'),(74,'articles','body',6,'ar','','2018-05-06 22:02:42','2018-05-06 22:02:42'),(75,'articles','author_name',6,'ar','','2018-05-06 22:02:42','2018-05-06 22:02:42'),(76,'articles','author_description',6,'ar','','2018-05-06 22:02:42','2018-05-06 22:02:42'),(77,'articles','title',7,'ar','','2018-05-07 20:29:31','2018-05-07 20:29:31'),(78,'articles','body',7,'ar','','2018-05-07 20:29:31','2018-05-07 20:29:31'),(79,'articles','author_name',7,'ar','','2018-05-07 20:29:31','2018-05-07 20:29:31'),(80,'articles','author_description',7,'ar','','2018-05-07 20:29:31','2018-05-07 20:29:31'),(81,'articles','title',9,'ar','','2018-05-07 20:31:26','2018-05-07 20:31:26'),(82,'articles','body',9,'ar','','2018-05-07 20:31:27','2018-05-07 20:31:27'),(83,'articles','author_name',9,'ar','','2018-05-07 20:31:27','2018-05-07 20:31:27'),(84,'articles','author_description',9,'ar','','2018-05-07 20:31:27','2018-05-07 20:31:27'),(85,'articles','title',10,'ar','','2018-05-07 20:36:54','2018-05-07 20:36:54'),(86,'articles','body',10,'ar','','2018-05-07 20:36:54','2018-05-07 20:36:54'),(87,'articles','author_name',10,'ar','','2018-05-07 20:36:54','2018-05-07 20:36:54'),(88,'articles','author_description',10,'ar','','2018-05-07 20:36:54','2018-05-07 20:36:54'),(89,'courses','title',1,'ar','لورم','2018-05-08 19:56:17','2018-05-08 19:56:17'),(90,'courses','text',1,'ar','لورم','2018-05-08 19:56:17','2018-05-08 19:56:17'),(91,'courses','user',1,'ar','لورم','2018-05-08 19:56:17','2018-05-08 19:56:17'),(92,'stories','title',1,'ar','تيست','2018-05-08 21:22:49','2018-05-08 21:24:48'),(93,'stories','description',1,'ar','تيست تبيت','2018-05-08 21:22:49','2018-05-08 21:24:48'),(94,'packages','title',1,'ar','','2018-05-08 22:32:15','2018-05-08 22:32:15'),(95,'packages','name',1,'ar','','2018-05-08 22:32:15','2018-05-08 22:32:15'),(96,'packages','button_text',1,'ar','','2018-05-08 22:32:15','2018-05-08 22:32:15'),(97,'packages','text1',1,'ar','','2018-05-08 22:32:15','2018-05-08 22:32:15'),(98,'packages','text2',1,'ar','','2018-05-08 22:32:15','2018-05-08 22:32:15'),(99,'packages','text3',1,'ar','','2018-05-08 22:32:15','2018-05-08 22:32:15'),(100,'packages','text4',1,'ar','','2018-05-08 22:32:16','2018-05-08 22:32:16'),(101,'packages','text5',1,'ar','','2018-05-08 22:32:16','2018-05-08 22:32:16'),(102,'menu_items','title',14,'ar','','2018-05-11 21:21:18','2018-05-11 21:21:18'),(103,'menu_items','title',29,'ar','','2018-05-11 21:22:56','2018-05-11 21:22:56'),(104,'menu_items','title',15,'ar','','2018-05-11 21:24:52','2018-05-11 21:24:52'),(105,'menu_items','title',16,'ar','','2018-05-11 21:26:17','2018-05-11 21:26:17'),(106,'menu_items','title',17,'ar','','2018-05-11 21:26:39','2018-05-11 21:26:39'),(107,'menu_items','title',30,'ar','','2018-05-11 21:28:32','2018-05-11 21:28:32'),(108,'menu_items','title',18,'ar','','2018-05-11 21:29:30','2018-05-11 21:29:30'),(109,'menu_items','title',19,'ar','','2018-05-11 21:29:52','2018-05-11 21:29:52'),(110,'menu_items','title',20,'ar','','2018-05-11 21:31:31','2018-05-11 21:31:31'),(111,'menu_items','title',31,'ar','','2018-05-11 21:32:04','2018-05-11 21:32:04'),(112,'menu_items','title',21,'ar','','2018-05-11 21:32:43','2018-05-11 21:32:43'),(113,'menu_items','title',22,'ar','','2018-05-11 21:33:06','2018-05-11 21:33:06'),(114,'menu_items','title',32,'ar','','2018-05-11 21:33:37','2018-05-11 21:33:37'),(115,'menu_items','title',23,'ar','','2018-05-11 21:34:57','2018-05-11 21:34:57'),(116,'menu_items','title',24,'ar','','2018-05-11 21:35:42','2018-05-11 21:35:42'),(117,'menu_items','title',25,'ar','','2018-05-11 21:36:05','2018-05-11 21:36:05'),(118,'menu_items','title',26,'ar','','2018-05-11 21:36:41','2018-05-11 21:36:41'),(119,'menu_items','title',27,'ar','','2018-05-11 21:37:06','2018-05-11 21:37:06'),(120,'menu_items','title',28,'ar','','2018-05-11 21:37:27','2018-05-11 21:37:27');

/*Table structure for table `user_roles` */

DROP TABLE IF EXISTS `user_roles`;

CREATE TABLE `user_roles` (
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_roles_user_id_index` (`user_id`),
  KEY `user_roles_role_id_index` (`role_id`),
  CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `user_roles` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned DEFAULT '2',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`role_id`,`name`,`email`,`avatar`,`password`,`remember_token`,`settings`,`created_at`,`updated_at`,`company`,`address`,`city`,`state`,`postal_code`,`country`,`mobile`,`fax`) values (1,3,'Admin','admin@world-tree.co','users/default.png','$2y$10$67fUBtCtB.L8RvIFStLpJegF6VrFByGEyfmg3r98KucH0tUyA.K72','mxN00fZEAK7t8AGAGYhU6iIowcbpwxblsbo967RS62zm2CeRECgK6Cv9SZ5F','{\"locale\":\"en\"}','2018-04-28 11:03:43','2018-05-11 21:46:51',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,1,'Mahmoud Elshafaey','mahmoud@world-tree.co','users/default.png','$2y$10$U9C31YWg9La0cH8YQJTfMO0hygcNKiZ3AoFL..d96bEmz5.qJxF1q','ypcI0eOo8xyAWOAAJ62ZTjPHQk6r4jADIQMlBbfukmZNOCJhZ9VkoJufD9S6','{\"locale\":\"en\"}','2018-05-07 21:49:33','2018-05-11 21:46:02',NULL,'Maadi','Cairo','C','11511','Egypt','01029786046','01111');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
