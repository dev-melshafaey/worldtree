<?php

use App\User;
use Faker\Generator as Faker;

$factory->define(App\Notification::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'body' => $faker->text(200),
        'user_id' =>User::all()->random()->id,
    ];
});
