<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->timestamp("last_seen")->nullable();
            $table->tinyInteger("is_online")->default(0)->nullable();
            $table->tinyInteger("is_active")->default(1)->nullable();
            $table->text("about")->nullable();
            $table->string("photo_url")->nullable();
            $table->string("activation_code")->nullable();
            $table->tinyInteger("is_system")->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn("last_seen");
            $table->dropColumn("is_online");
            $table->dropColumn("is_active");
            $table->dropColumn("about");
            $table->dropColumn("photo_url");
            $table->dropColumn("activation_code");
            $table->dropColumn("is_system");
        });
    }
}
